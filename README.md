# Steelseries Integration

### SteelSeries peripherals RGB control integrated with different games/software

#### Supports the following peripherals:
  - SteelSeries Apex keyboards (103 RGB zones, OLED screen)
  - SteelSeries QCK Prism mousepads (2 RGB zones)
  - SteelSeries Rival 600 mouse (8 RGB zones)
  
#### Supports the following games/software:
  - **CS:GO**
  - **Spotify**

#### Planning to support:
  - **Valorant** (when Riot will release their API)

### References:
 - [SteelSeries's official API documentation](https://github.com/SteelSeries/gamesense-sdk)



# CS:GO Integration

### Peripherals RGB lighting reacts to in-game events

#### Supports the following in-game events:
  - **Bomb planted** - Constant red flash at low frequency
  - **Bomb defused** - Short blue flash at high frequency
  - **Bomb explosion** - Short red flash at high frequency
  - **Player getting flashed** - White illumination corresponding to the amount of player blindness
  - **Buy binds** - Buy binds keys light red, orange or green depending on the amount of money the player currently has and which item is present in their inventory. *Only works during freezetime*. Supports CT/T sides economies.

### References:
 - [Official CS:GO GSI documentation](https://developer.valvesoftware.com/wiki/Counter-Strike:_Global_Offensive_Game_State_Integration)
 - [Reddit post detailing GSI functionalities **(recommended)**](https://www.reddit.com/r/GlobalOffensive/comments/cjhcpy/game_state_integration_a_very_large_and_indepth/)



# Spotify Integration

### Controls SteelSeries keyboards OLED screen to display information about currently playing track and responds to key presses

#### Spotify control:
  - **Volume control** *(default: F23, F24)* increment/decrement the volume of Spotify only. *Premium Spotify users only!*
  - **Save track** *(default: F21)* save the current track to your Spotify library

#### App registeration:
In order to use this script you need to get **a OAuth Token** from Spotify to authorize/identify your account when making requests.
This token can only be generated if you register an app to work with it:
  - Go to the [Spotify developper portal and log in](https://developer.spotify.com/dashboard/)
  - Create a new app
  - Set your [redirect URLs](https://i.imgur.com/lpQgZl0.png) in the app settings. The redirect URL can be whatever URL/port. **It is recommended to use localhost/127.0.0.1**
  - Copy both your *Client ID* and *Client secret* in a JSON file just like *example_spotify_token.oauth* and rename it *spotify_token.oauth*
  - Set the same redirect URL in your JSON OAuth file as configured on the Twitch developer portal

#### References:
  - [Official Spotify API documentation](https://developer.spotify.com/documentation/)
  - [Spotipy, a Spotify Python package](https://spotipy.readthedocs.io/en/2.12.0/)



# Twitch Integration

### Watch streamers and get notified when they go live

#### Twitch notifications:
  - **Streamer goes live**: shows a notification when a watched channel goes live *(username, current game, stream title)*
  - **Streamer changes game**: shows a notification when a watched channel changes game *(username, current game, stream title)*
  - **Followers**: shows a notification when a user follows a watched channel *(follower, followee)*

#### App registeration:
In order to use this script you need to get **a OAuth Token** from Spotify to authorize/identify your account when making requests.
This token can only be generated if you register an app to work with it:
  - Go to the [Twitch developper portal and log in](https://dev.twitch.tv/console/apps)
  - Create a new app
  - Set your [redirect URLs](https://i.imgur.com/ezk9DuY.png) in the app settings. **The redirect URL must match your home IP address (or a DynDNS pointing towards it). The default port is 5555**
  - Copy both your *Client ID* and *Client secret* in a JSON file just like *example_twitch_token.oauth* and rename it *twitch_token.oauth*
  - Set the same redirect URL in your JSON OAuth file as configured on the Twitch developer portal

#### Important note:
To receive asynchronous HTTP POST requests from Twitch you need to have a server running, therefore you need to open/redirect the chosen port *(default 5555)* in your router and firewall to let the connections through.


#### References:
  - [Official Twitch API documentation](https://dev.twitch.tv/docs/)



# Windows Audio (CURRENTLY NOT WORKING)

### Retrieves Windows master volume and interacts with OLED screen

#### References:
  - [Stackoverflow](https://stackoverflow.com/questions/32149809/read-and-or-change-windows-8-master-volume-in-python)