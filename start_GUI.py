# other project files
from src.Modules.CSGO.csgo import CSGO
from src.Modules.Twitch.twitch_listener import TwitchListener
from src.Modules.Twitch.twitch import Twitch
from src.Modules.Spotify.spotify import Spotify
from src.Modules.Background.background import Background
from src.Modules.Background.weather import Weather
from src.Modules.Logitech.logitech import Logitech
from src.WebServer import WebServer
from src.utils.KeyboardListener import KeyboardListener
from src.utils.CallQueue import CallQueue
from src.utils.utils import log

from time import sleep

import threading

from functools import reduce
import operator

# Flask server
import flask
from flask import request
from flask_socketio import SocketIO

# modules to use
enable_CSGO = False
enable_SPOTIFY = True
enable_WINAUDIO = False
enable_BACKGROUND = False
enable_WEATHER = True
enable_TWITCH = True
enable_LOGITECH = True

if __name__ == "__main__":
	# shared queue between threads
	queue = CallQueue()
	threads = []

	# keyboard input listener
	listener = KeyboardListener(threads)
	listener.start()

	# start dashboard webpage server
	dashboard_thread = WebServer(queue)
	dashboard_thread.start()
	threads.append(dashboard_thread)

	if enable_CSGO:
		# CSGO server config
		csgo_app = flask.Flask("CSGO event listener")
		csgo_app.config["DEBUG"] = False

		# Receives CSGO game events generated with CSGO GSI
		@csgo_app.route("/csgo_game_events", methods=["GET","POST"])
		def csgo_event():
			return csgo_thread.incoming_data(request)

		# thread
		csgo_thread = CSGO(csgo_app, queue=queue)
		csgo_thread.start()
		threads.append(csgo_thread)

	if enable_TWITCH:
		# Server config
		twitch_app = flask.Flask("Twitch event listener")
		twitch_app.config["DEBUG"] = False

		# receives Twitch authentification requests
		@twitch_app.route("/twitch", methods=["GET"])
		def auth_request():
			return twitch_listener_thread.incoming_data(request)

		# receives Twitch events generated with webhooks
		@twitch_app.route("/twitch/<topic>/<username>", methods=["GET", "POST"])
		def webhooks(topic, username):
			return twitch_listener_thread.incoming_data(request, topic, username)

		# thread
		twitch_thread = Twitch(queue=queue)
		twitch_thread.start()

		twitch_listener_thread = TwitchListener(twitch_app, twitch_thread, queue=queue)
		twitch_listener_thread.start()
		threads.append(twitch_thread)

	if enable_SPOTIFY:
		# thread
		spotify_thread = Spotify(queue=queue)
		spotify_thread.start()
		threads.append(spotify_thread)

	if enable_BACKGROUND:
		# thread
		bg_thread = Background(queue=queue)
		bg_thread.start()
		threads.append(bg_thread)

	if enable_WEATHER:
		# thread
		weather_thread = Weather(queue=queue)
		weather_thread.start()
		threads.append(weather_thread)

	if enable_LOGITECH:
		# thread
		logitech_thread = Logitech(queue=queue)
		logitech_thread.start()
		threads.append(logitech_thread)

	queue.start()