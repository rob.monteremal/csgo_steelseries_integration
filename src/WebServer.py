import threading
import copy
from random import randint
from flask_socketio import SocketIO
import flask
from src.utils.utils import log
import sys
sys.path.append("..\\..")


class WebServer(threading.Thread):

    def __init__(self, queue):
        threading.Thread.__init__(self)

        self.queue = queue
        queue.add_thread(self)
        self.app = flask.Flask(
            "Dashboard", template_folder="Web\\templates", static_folder="Web\\static")
        self.socketio = SocketIO(self.app)
        self.config_app()

        # load default web data
        import json
        with open("Web\\templates\\index.json", "r") as template_data:
            self.web_data = json.load(template_data)

    def config_app(self):
        self.app.config['host'] = "127.0.0.1"
        self.app.config['port'] = 5000
        self.app.config["TEMPLATES_AUTO_RELOAD"] = True

        self.setup_flask_routes()
        self.setup_socketio_routes()

    def setup_flask_routes(self):
        @self.app.route("/dashboard", methods=["GET"])
        def home(): return self.home()

    def setup_socketio_routes(self):
        self.socketio.on_event("spotify-update", self.spotify_update)
        self.socketio.on_event("start", self.ready)
        self.socketio.on_event("notification-clear", self.delete_notification)

    def home(self):
        headers = {'Content-Type': 'text/html'}
        return flask.render_template("index.html", web_data=self.web_data, headers=headers)

    # sends the current web_data to the page after page is loaded
    def ready(self, data=None):
        self.update_webpage(self.web_data)

    # removes a notification from the database given
    def delete_notification(self, id_):
        item = None
        try:
            notifications = self.web_data["notifications"]
            for i, notification in enumerate(notifications):
                if int(notification["id"]) == int(id_):
                    item = notifications.pop(i)

            if not(item):
                raise Exception()
        except Exception as e:
            log("Couldn't find notification with id "+str(id_), self)
            log(e, self)

    def update_webpage(self, data):
        log("Sending: "+str(data), self)
        self.socketio.emit("update", data)

    def spotify_update(self, data):
        log("Received: "+str(data), self)
        self.queue.put("webserver", "spotify", data)

    # receives keyboard events from KeyboardListener
    def keyboard_event(self, key):
        pass

    # receives events sent through the queue to this thread
    def queue_event(self, source, raw_data):
        data = {}
        # log(raw_data)
        if source == "spotify":
            for key in ["player", "playlists", "progress", "controls"]:
                if raw_data.get(key, None) and raw_data.get(key, None) != self.web_data.get(source, {}).get(key, None):
                    data.update({key: raw_data[key]})
                    self.web_data[source][key] = raw_data[key]
        elif source == "weather":
            self.web_data.update({source: raw_data})
            data = raw_data
        elif source == "logitech":
            self.web_data.update({source: raw_data})
        elif source == "ping":
            pass
        elif source == "monitoring":
            pass
        elif source == "notifications":
            raw_data.update({"id": randint(1, 1000000)})
            self.web_data.get(source).append(raw_data)
            data = [raw_data]
        else:
            log(source+" isn't supported by the dashboard")
            return None

        # data has ACTUALLY changed
        if data:
            # log("DATA TO UPDATE: "+str(data))
            self.update_webpage({source: data})

    def run(self):
        self.socketio.run(self.app)
