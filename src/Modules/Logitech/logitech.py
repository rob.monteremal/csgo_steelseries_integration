# other project files
import threading
from time import sleep
import json
from src.utils.utils import log
import sys
import os
sys.path.append("..\\..")


# multithreading


class Logitech(threading.Thread):
    FILE_LOCATION = os.getenv("APPDATA") + "\\..\\Local\\LGHUB\\settings.json"

    devices = ["prowirelessmouse", "proxwirelessheadset"]

    def __init__(self, queue=None):
        # create thread
        threading.Thread.__init__(self)

        self.queue = queue
        if queue:
            queue.add_thread(self)

    # reads Logitech settings.json
    def get_battery_levels(self, battery_levels):
        try:
            with open(self.FILE_LOCATION, "r") as settings_file:
                for level in battery_levels.values():
                    level["on"] = False
                settings = json.load(settings_file)
                for setting in settings:
                    if setting.startswith("battery"):
                        values = setting.split("/")
                        if values[2] == "percentage" and len(values) == 3:
                            battery_levels[values[1]] = self.format_battery(
                                settings[setting])

                self.queue.put("logitech", "webserver", battery_levels)
                return battery_levels
        except Exception as e:
            log("Error: The settings.json file cannot be opened", self)
            log(e, self)
            return battery_levels

    def format_battery(self, values):
        return {
            "battery": values["percentage"],
            "charging": values.get("isCharging", False),
            "on": True
        }

    def init_battery_levels(self):
        return {device: {"on": False} for device in self.devices}

    # receives keyboard events from KeyboardListener
    def keyboard_event(self, key):
        return None

    def run(self):
        log("Starting thread", self)
        battery_levels = self.init_battery_levels()

        while(True):
            battery_levels = self.get_battery_levels(battery_levels)
            sleep(60)
