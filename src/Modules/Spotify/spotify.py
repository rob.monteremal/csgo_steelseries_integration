import threading
from datetime import datetime as dt
import time
import requests
import json
from src.utils.utils import log
from src.Modules.Spotify import spotify_auth
from src.SteelSeries.pipeline import EventPipeline
import src.SteelSeries.steelseries as ss
import sys
sys.path.append("..\\..")


class Spotify(threading.Thread):
    # Change to True if you modified any event in the event list
    FORCE_EVENT_UPDATE = False

    GAME = "SPOTIFY_CUSTOM"
    EVENT_LIST = {
        "PLAY": {
            "lines": ["artist", "track"],
            "bar": True,
            "icon": 0,
            "priority": 108
        },
        "PAUSE": {
            "lines": ["artist", "track"],
            "bar": True,
            "icon": 25,
            "priority": 108
        },
        "SAVE_TRACK": {
            "lines": ["text"],
            "bar": False,
            "icon": 26,
            "priority": 3
        },
        "VOLUME": {
            "lines": ["text"],
            "bar": True,
            "icon": 26,
            "priority": 5
        }
    }
    VOL_INCREMENT = 5

    # constructor
    def __init__(self, pipeline=(0, None), queue=None):
        # create thread
        threading.Thread.__init__(self)
        self.addr = pipeline[0]
        self.pipeline = pipeline[1]

        self.queue = queue
        if queue:
            queue.add_thread(self)

        self.set_auth()
        self.player_info = {}

        if self.FORCE_EVENT_UPDATE:
            # sets up SteelSeries events
            ss.unbind_all_event(self.GAME, self.EVENT_LIST, self.addr)
            for event_name, event in self.EVENT_LIST.items():
                ss.bind_oled_event(self.GAME, event_name, ss.create_oled_data(
                    event["lines"], event["bar"], event["icon"]), self.addr)

    # loads Spotify OAuth token from a JSON file
    def get_oauth_token(self, file_name, force_generate=False):
        # generate new token (token expired or scope changed)
        if(force_generate):
            scope = "user-modify-playback-state"  # set volume
            scope += " user-read-currently-playing"  # currently playing
            scope += " user-read-playback-state"  # player info (volume)
            scope += " user-library-modify"  # save tracks
            scope += " user-top-read"  # TEST
            spotify_auth.generate_token(scope, file_name, self)

        # load token from file
        try:
            with open(file_name) as file:
                token = json.load(file).get("token")

            if type(token) == str and token != "":
                log("Token successfully loaded!", self)

        except Exception as e:
            log(e, self)
            token = ""

        return token

    # sets the auth token
    def set_auth(self):
        self.token = self.get_oauth_token("config\\spotify_token.oauth", True)

    def get_auth_header(self):
        return {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer "+self.token
        }

    def send_spotify(self, endpoint, params=None, data=None, method="GET"):
        base_URL = "https://api.spotify.com"

        if endpoint.startswith(base_URL):
            url = endpoint
        else:
            url = base_URL+endpoint

        try:
            if method == "GET":
                r = requests.get(
                    url, headers=self.get_auth_header(), params=params)
            elif method == "POST":
                r = requests.post(
                    url, headers=self.get_auth_header(), params=params, data=data)
            elif method == "PUT":
                r = requests.put(
                    url, headers=self.get_auth_header(), params=params, json=data)
            elif method == "DELETE":
                r = requests.delete(
                    url, headers=self.get_auth_header(), params=params)
        except:
            log("Error while trying to send request", self)
            return {}

        if not(200 <= r.status_code < 300):
            # Authentification failed (expired)
            if r.status_code == 401:
                self.set_auth()
                return self.send_spotify(endpoint, params=params, data=data, method=method)
            else:
                log("Error: %d while sending %s %s" %
                    (r.status_code, method, endpoint), self)
                try:
                    log(r.json(), self)
                except:
                    pass
                return {}

        # No content
        if r.status_code == 204:
            return {}

        try:
            return r.json()
        except:
            return {}

    # returns a list of the user's playlist
    def get_playlists(self):
        playlists = []

        try:
            data = self.send_spotify(
                "/v1/me/playlists", params={"limit": 50}, method="GET")
            playlists.extend([{"name": item.get("name", "Unknown playlist"), "uri": item.get(
                "uri", None)} for item in data.get("items")])

            while(data.get("next", None)):
                data = self.send_spotify(data["next"])
                playlists.extend([{"name": item.get("name", "Unknown playlist"), "uri": item.get(
                    "uri", None)} for item in data.get("items", [])])

        except:
            log("Error while retrieving playlists", self)

        return playlists

    # returns raw info about currently playing track
    def get_currently_playing(self):
        return self.send_spotify("/v1/me/player", method="GET")

    # returns JSON containing useful information about currently playing track
    def get_currently_playing_track(self):
        data = self.get_currently_playing()
        if not(data):
            return None

        try:
            vol = data.get("device", {}).get("volume_percent", 0)
            device_id = data.get("device", {}).get("id", None)
            out_data = {
                "artist": data.get("item", {}).get("artists", [{}])[0].get("name", "Unknown Artist"),
                "album": data.get("item", {}).get("album", {}).get("name", "Unknown Album"),
                "track": data.get("item", {}).get("name", "Unknown Track"),
                "cover": data.get("item", {}).get("album", {}).get("images", [{}])[0].get("url", ""),
                "duration": data.get("item", {}).get("duration_ms", 1),
                "time": data.get("progress_ms", 0),
                "id": data.get("item", {}).get("id", 0),
                "volume": vol,
                "loop": "loop_"+data.get("repeat_state", "off")
            }
            if data.get("shuffle_state", False):
                out_data["shuffle"] = "shuffle_on"
            else:
                out_data["shuffle"] = "shuffle_off"

            if data.get("is_playing", False):
                out_data["is_playing"] = "PLAY"
            else:
                out_data["is_playing"] = "PAUSE"

            # only update volume if non-zero (player isn't muted)
            if vol:
                self.player_info["volume"].update({"value": vol})
            self.player_info["volume"].update({"muted": not(bool(vol))})

            self.player_info.update({"device_id": device_id})

            return out_data
        except:
            log("Error retrieving song data", self)
            return {}

    # returns ID of currently playing track
    def get_currently_playing_id(self):
        data = self.get_currently_playing()

        return data.get("item", {}).get("id", 0)

    # returns the volume of the playback
    def get_volume(self):
        try:
            return self.send_spotify("/v1/me/player", method="GET")["device"]["volume_percent"]
        except:
            return None

    # sets the volume of the playback
    def set_volume(self, value=0):
        # percentage bounds
        if value >= 100:
            value = 100
        elif value < 0:
            value = 0

        self.send_spotify("/v1/me/player/volume",
                          params={"volume_percent": value}, method="PUT")

        # only change volume if there is a non-zero value
        # else we are muting the player and want to still save the last volume
        if value:
            self.player_info.get("volume", {}).update({"value": value})
        self.player_info.get("volume", {}).update({"muted": bool(value)})

        if self.addr:
            event_name = "VOLUME"
            event = self.pipeline.craft_event(self.GAME, event_name, self.EVENT_LIST[event_name]["priority"], 1, value, {
                                              "text": "Volume: "+str(value)+"%"})
            self.pipeline.add_event(event)

    # increments volume by VOL_INCREMENT
    def increment_volume(self):
        volume = self.player_info.get("volume", {}).get("value", 0)
        if volume != None:
            return self.set_volume(volume+self.VOL_INCREMENT)

    # decrements volume by VOL_INCREMENT
    def decrement_volume(self):
        volume = self.player_info.get("volume", {}).get("value", 0)
        if volume != None:
            return self.set_volume(volume-self.VOL_INCREMENT)

    # checks whether a track is in the user's tracks library
    def get_saved_status(self, id):
        if not(id):
            return False
        try:
            return self.send_spotify("/v1/me/tracks/contains", params={"ids": [id]}, method="GET")[0]
        except:
            return False

    # saves/mark as favorite current track
    def save_unsave_track(self, id=None, save=True):
        # if no ID is provided we can the currently playing track ID
        if not(id):
            id = self.get_currently_playing_id()
        if not(id):
            return None

        if save:
            self.send_spotify(
                "/v1/me/tracks", params={"ids": [id]}, method="PUT")
        else:
            self.send_spotify(
                "/v1/me/tracks", params={"ids": [id]}, method="DELETE")

        if self.addr:
            event_name = "SAVE_TRACK"
            event = self.pipeline.craft_event(
                self.GAME, event_name, self.EVENT_LIST[event_name]["priority"], 1, 0, {"text": "Saved to library!"})
            self.pipeline.add_event(event)

    def send_action(self, endpoint, params={}, data=None, method="PUT"):
        # sets the device ID for playback
        if self.player_info.get("device_id", None):
            params.update({"device_id": self.player_info["device_id"]})

        self.send_spotify("/v1/me/"+endpoint, params=params,
                          data=data, method=method)

    def perform_action(self, action_type, action_data=None):
        if action_type == "pause":
            self.send_action("player/pause")
        elif action_type == "play":
            self.send_action("player/play")
        elif action_type == "next":
            self.send_action("player/next", method="POST")
        elif action_type == "previous":
            self.send_action("player/previous", method="POST")
        elif action_type == "loop_off":
            self.send_action("player/repeat", params={"state": "off"})
        elif action_type == "loop_context":
            self.send_action("player/repeat", params={"state": "context"})
        elif action_type == "loop_track":
            self.send_action("player/repeat", params={"state": "track"})
        elif action_type == "shuffle_on":
            self.send_action("player/shuffle", params={"state": "true"})
        elif action_type == "shuffle_off":
            self.send_action("player/shuffle", params={"state": "false"})
        elif action_type == "toggle_mute":
            # player isn't muted
            if(self.player_info.get("volume", {}).get("muted", False)):
                self.set_volume(self.player_info.get(
                    "volume", {}).get("value", 0))
                self.player_info.get("volume", {})["muted"] = False
            else:
                self.set_volume(0)
                self.player_info.get("volume", {})["muted"] = True
        elif action_type == "uri":
            self.send_action(
                "player/play", data={"context_uri": str(action_data)})
        elif action_type == "save":
            self.save_unsave_track(save=True)
        elif action_type == "unsave":
            self.save_unsave_track(save=False)
        else:
            log("Action "+action_type+" not supported", self)

    # returns song progress percentage
    def progress(self, time, duration):
        return (time / duration)*100

    def icon(self, is_playing):
        if is_playing == "PLAY":
            return "pause"
        elif is_playing == "PAUSE":
            return "play"
        # safety
        else:
            return "play"

    def saved(self, is_saved):
        if is_saved:
            return "unsave"
        else:
            return "save"

    # receives keyboard events from KeyboardListener
    def keyboard_event(self, key):
        if key == "f21":
            self.save_unsave_track(save=True)
        elif key == "f23":
            self.increment_volume()
        elif key == "f24":
            self.decrement_volume()

    # receives events sent through the queue to this thread
    def queue_event(self, source, raw_data):
        if raw_data.get("action", None):
            self.perform_action(raw_data["action"])
        if raw_data.get("volume_percent", None):
            self.set_volume(int(raw_data["volume_percent"]))
        if raw_data.get("progress_percent", None):
            seek_time = self.current_data["duration"] * \
                raw_data["progress_percent"]/100
            self.send_action("player/seek", {"position_ms": int(seek_time)})
        if raw_data.get("uri", None):
            self.perform_action("uri", raw_data["uri"])

    def run(self):
        log("Starting thread", self)
        vol = self.get_volume()

        timestamp_playlist = 0

        self.current_data = {}
        self.player_info.update(
            {"volume": {"value": vol, "muted": not(bool(vol))}})

        fetch_rate = 0.5    # time in sec between track fecth
        shift_rate = 3      # number of cycles between text shift
        shift_count = 0

        shifts = {
            "artist": {},
            "track": {}
        }

        while(True):
            t_start = time.time()
            new_data = self.get_currently_playing_track()

            # only update if proper info has been recieved
            if new_data:
                # track has changed
                if new_data["track"] != self.current_data.get("track"):
                    new_data["is_saved"] = self.get_saved_status(
                        new_data["id"])
                else:
                    new_data["is_saved"] = self.current_data.get(
                        "is_saved", False)

                self.current_data = new_data
                # log(new_data)
                # STEELSERIES
                if self.addr:
                    # track changed
                    if new_data["artist"] != data.get("artist", "") or new_data["track"] != data.get("track", ""):
                        shifts = ss.new_text_wrap_word(new_data, shifts)
                    else:
                        shift_count += 1
                        if shift_count >= shift_rate:
                            shifts = ss.text_wrap_word_all(shifts)
                            shift_count = 0

                    priority = self.EVENT_LIST[new_data["is_playing"]]["priority"]
                    # changed from play to pause or vice versa, higher priority event
                    if new_data["is_playing"] != data.get("is_playing", ""):
                        priority -= 2

                    data = new_data
                    event = self.pipeline.craft_event(self.GAME, data["is_playing"], priority, 3, int(self.progress(
                        data["time"], data["duration"])), {"artist": shifts["artist"]["text"], "track": shifts["track"]["text"]})
                    self.pipeline.add_event(event)

                # GUI
                if self.queue:
                    data = {
                        "player": {
                            "track_name": new_data["track"],
                            "artist_name": new_data["artist"],
                            "album_name": new_data["album"],
                            "album_cover": new_data["cover"]
                        },
                        "progress":	{
                            "time": new_data["time"]/1000,
                            "duration": new_data["duration"]/1000,
                            "progress": self.progress(new_data["time"], new_data["duration"])
                        },
                        "controls":	{
                            "shuffle": new_data["shuffle"],
                            "loop": new_data["loop"],
                            "volume": new_data["volume"],
                            "icon": self.icon(new_data["is_playing"]),
                            "save": self.saved(new_data["is_saved"])
                        }
                    }
                    self.queue.put("spotify", "webserver", data)

                    # request playlists only once/minute
                    if (dt.now().timestamp() - timestamp_playlist) > 60:
                        playlists = self.get_playlists()
                        timestamp_playlist = dt.now().timestamp()
                        self.queue.put("spotify", "webserver", {
                                       "playlists": playlists})

            if self.addr:
                # keep the keyboard communication alive
                ss.heartbeat(self.GAME, self.addr)

            # prevents Spotify API spam
            sleep_time = fetch_rate - time.time() + t_start
            if sleep_time < 0:
                sleep_time = 0
            time.sleep(sleep_time)
