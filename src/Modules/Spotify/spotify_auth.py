from src.utils.utils import log

import spotipy
import spotipy.util as util
import json

# generates spotify token using credentials from token_file
# if the token is expired, a web page opens up to acquire a new token
def generate_token(scope, token_file, thread):
	try:
		with open(token_file, "r") as file:
			auth = json.load(file)

		# update with new token value
		token = util.prompt_for_user_token(auth["username"], scope, client_id=auth["CLIENT_ID"], client_secret=auth["CLIENT_SECRET"], redirect_uri=auth["REDIRECT_URI"])
			
		if token != None:
			log("New token generated", thread)
			auth["token"] = token
		
		with open(token_file, "w") as file:
			json.dump(auth, file)

	except Exception as e:
		log(e)
		token = None

if __name__ == "__main__":
	generate_token("user-read-currently-playing user-library-modify", "spotify_token.oauth", None)
