import codecs
import pickle
import threading
from time import sleep
from comtypes import CoInitialize, CoUninitialize
from src.Modules.WindowsAudio.win_audio import IAudioEndpointVolume
from src.utils.utils import log
from src.SteelSeries.pipeline import EventPipelineThread
import src.SteelSeries.steelseries as ss
import sys
import os
sys.path.insert(0, ".")


class WinAudioWrapperThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)


class WinAudioWrapper(threading.Thread):
    GAME = "WINDOWS"
    EVENT_LIST = {
        "VOLUME": {
            "lines": ["text"],
            "bar": True,
            "icon": 27,
            "priority": 5
        },
        "MUTED": {
            "lines": ["text"],
            "bar": False,
            "icon": 19,
            "priority": 5
        }
    }

    def __init__(self, addr=None, pipeline=None):
        threading.Thread.__init__(self)
        self.set_endpoint()
        self.current_volume = self.get_current_windows_volume()

        if addr != None:
            self.addr = addr
        else:
            self.addr = ss.get_addr()

        # as thread
        if pipeline != None:
            self.pipeline = pipeline
        # load pipeline from subprocess PIPE via pickle
        else:
            pass

        ss.unbind_all_event(self.GAME, self.EVENT_LIST, addr)
        for event_name, event in self.EVENT_LIST.items():
            ss.bind_oled_event(self.GAME, event_name, ss.create_oled_data(
                event["lines"], event["bar"], event["icon"]), addr)

    # sets the endpoint to the current default Windows sound output
    def set_endpoint(self):
        try:
            CoInitialize()
            self.ev = IAudioEndpointVolume.get_default()
        except Exception as e:
            log("Error in %s" % (__name__), self)
            log(e, self)

    # returns the value (percentage) of the Windows default peripheral volume
    def get_current_windows_volume(self):
        try:
            vol, _ = self.ev.GetVolumeStepInfo()
            return 2*vol
        except Exception as e:
            log("Error in %s" % (__name__), self)
            log(e, self)
            return None

    # returns wether or not the sound is muted
    def is_muted(self):
        try:
            return self.ev.GetMute()
        except Exception as e:
            log("Error in %s" % (__name__), self)
            log(e, self)
            return False

    # receives keyboard events from KeyboardListener
    def keyboard_event(self, key):
        # if the audio output was changed
        if (key == "f15" or key == "f16") and self.cont:
            self.stop()
            sleep(1)  # wait for input switch
            # CoUninitialize()
            #self.ev = None
            self.set_endpoint()
            self.resume()

    # pause the thread
    def stop(self):
        self.cont = False
        log("Stopped thread", self)

    # resumes the thread
    def resume(self):
        self.cont = True
        log("Resuming thread", self)

    # constantly checks the volume of Windows
    def run(self):
        self.cont = True
        log("Starting thread", self)
        while(self.cont):
            # keep the keyboard communication alive
            ss.heartbeat(self.GAME, self.addr)
            try:
                volume = self.get_current_windows_volume()
                if self.is_muted() or volume == 0:
                    self.current_volume = 0
                    event_name = "MUTED"
                    event = self.pipeline.craft_event(
                        self.GAME, event_name, self.EVENT_LIST[event_name]["priority"], 1, volume, {"text": "Sound muted"})
                    self.pipeline.add_event(event)
                # only update the screen if the volume changed
                elif volume != self.current_volume:
                    self.current_volume = volume
                    event_name = "VOLUME"
                    event = self.pipeline.craft_event(self.GAME, event_name, self.EVENT_LIST[event_name]["priority"], 1, volume, {
                                                      "text": "Volume: "+str(volume)+"%"})
                    self.pipeline.add_event(event)
            except Exception as e:
                log("Error in %s" % (__name__), self)
                log(e, self)

            # prevents SteelSeries API spam
            sleep(0.1)
        CoUninitialize()


if __name__ == "__main__":
    log("Starting WinAudio subprocess")
    #pipeline = pickle.load(sys.stdin.buffer)
    print(sys.argv)
    pipeline_pickeled = sys.argv[0].encode()
    win_audio_thread = WinAudioWrapper(pipeline=pickle.loads(
        codecs.decode(pipeline_pickeled, "base64")))
    win_audio_thread.start()
    pickle.dump(win_audio_thread, sys.stdout.buffer)
