import hashlib
import hmac
import threading
import logging
from flask import request, Response
import copy
from time import time
import requests
import json
from src.utils.utils import log
from src.Modules.Twitch.twitch_auth import answer_auth
import src.SteelSeries.steelseries as ss
from src.SteelSeries.pipeline import EventPipeline
import sys
sys.path.append("..\\..")


# Logger
debug = False
logger = logging.getLogger("werkzeug")
logger.setLevel(logging.ERROR)
logger.disabled = debug


class TwitchListener(threading.Thread):

    def __init__(self, app, twitch, pipeline=(0, None), queue=None):
        threading.Thread.__init__(self)
        self.addr = pipeline[0]
        self.pipeline = pipeline[1]
        self.twitch = twitch

        # Twitch server config
        self.host = "0.0.0.0"
        self.port = 5555
        self.app = app

    # triggered each time a new HTTP request is received
    def incoming_data(self, request, topic=None, username=None):
        raw_data = request.get_data()
        try:
            data = request.json
            # the secret doesn't match => request is rejected
        except Exception as e:
            # log(data, self)
            log(e, self)
            data = {}
            return Response(status=400)

        # check the signature
        signed = False
        signature = request.headers.get("x-hub-signature", "=").split("=")[-1]
        hash = hmac.new(str.encode(self.client_secret),
                        msg=raw_data, digestmod=hashlib.sha256).hexdigest()
        if hash == signature:
            signed = True

        # auth request
        if not(topic):
            if request.args.get("code", None):
                if not(answer_auth("twitch", request.args.get("code", None))):
                    log("Error generating token!", self)
                return Response(status=202)
        else:
            # challenge request
            if request.args.get("hub.challenge", None):
                return self.answer_challenge(request.args.get("hub.challenge", None))
            # stream status notification
            elif signed:
                # log(data)
                if topic == "streams":
                    log("New notification", self)
                    self.twitch.channel_update(username, data)
                    return Response(status=202)
                elif topic == "users/follows":
                    log("New follower", self)
                    self.twitch.new_follower(username, data)
                    return Response(status=202)
            else:
                log("Signature could not be verified", self)
                log("Expected: "+hash+" received: "+signature, self)
                return Response(status=401)

    # Responds to a Twitch challenge by returning the code sent
    def answer_challenge(self, challenge):
        return Response(str(challenge), status=200, mimetype="text/plain")

    # receives keyboard events from KeyboardListener
    def keyboard_event(self, key):
        pass

    # sets the client secret used as a secret
    def set_secret(self):
        try:
            with open("config\\twitch_token.oauth", "r") as token_file:
                auth = json.load(token_file)
                self.client_secret = auth["client_secret"]
                return True
        except Exception as e:
            log("Error: The OAuth file doesn't exist!", self)
            log(e, self)
            return False

    def run(self):
        log("Starting thread", self)
        log("Twitch listener server running on %s:%s" %
            (self.host, self.port), self)
        self.set_secret()
        self.app.run(host=self.host, port=self.port)
