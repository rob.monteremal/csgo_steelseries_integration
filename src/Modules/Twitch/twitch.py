import threading
import logging
from flask import request
import flask
from time import time, sleep
import datetime as dt
import requests
import json
from src.utils.utils import log
from src.Modules.Twitch.twitch_auth import generate_token
from src.SteelSeries.pipeline import EventPipeline
import src.SteelSeries.steelseries as ss
import sys
sys.path.append("..\\..")


# Logger
debug = False
logger = logging.getLogger("werkzeug")
logger.setLevel(logging.ERROR)
logger.disabled = debug


class Twitch(threading.Thread):
    # Change to True if you modified any event in the event list
    FORCE_EVENT_UPDATE = False

    GAME = "TWITCH_CUSTOM"
    EVENT_LIST = {
        "STREAM": {
            "lines": ["channel", "game", "title"],
            "bar": False,
            "icon": 0,
            "priority": 3
        },
        "FOLLOW": {
            "lines": ["follower", "followee"],
            "bar": False,
            "icon": 0,
            "priority": 3
        }
    }

    SUB_LIST = {
        # ESPORTS
        "rainbow6": {"topic": ["streams"]},
        "valorant": {"topic": ["streams"]},
        "rocketleague": {"topic": ["streams"]},
        "esl_csgo": {"topic": ["streams"]},
        "esl_csgob": {"topic": ["streams"]},
        # REGULARS
        "lirik": {"topic": ["streams"]},
        "giantwaffle": {"topic": ["streams"]},
        "shortyyguy": {"topic": ["streams"]},
        "forsen": {"topic": ["streams"]},
        "pokelawls": {"topic": ["streams"]},
        "gmhikaru": {"topic": ["streams"]},
        "mistermv": {"topic": ["streams"]},
        "shroud": {"topic": ["streams"]},
        "lvndmark": {"topic": ["streams"]},
        "sodapoppin": {"topic": ["streams"]},
        "skadoodle": {"topic": ["streams"]},
        # SPEEDRUN
        "clintstevens": {"topic": ["streams"]},
        "linkus7": {"topic": ["streams"]},
        "kz_frew": {"topic": ["streams"]},
        "darkviper08": {"topic": ["streams"]},
        "friendlybaron": {"topic": ["streams"]},
        # RARE/DEAD
        "cdprojektred": {"topic": ["streams"]},
        "ravic": {"topic": ["streams"]},
        "tylerglaiel": {"topic": ["streams"]},
        "lethalfrag": {"topic": ["streams"]},
        "linustech": {"topic": ["streams"]},
        "luke_lafr": {"topic": ["streams"]},
        "sethbling": {"topic": ["streams"]},
        "bdoubleo": {"topic": ["streams"]},
        "hermanli": {"topic": ["streams"]},
        "hannahtelle18": {"topic": ["streams"]},
        # IRL/TEST
        "svek": {"topic": ["streams"]},
        "sevkiz": {"topic": ["streams"]},
        "smushis": {"topic": ["streams"]},
        "mejisto": {"topic": ["streams"]},
        "eleuzis": {"topic": ["streams"]},
        "mrbuk5": {"topic": ["streams"]},
        "rawb_": {"topic": ["streams", "users/follows"]}
    }

    def __init__(self, pipeline=(0, None), queue=None):
        threading.Thread.__init__(self)
        self.addr = pipeline[0]
        self.pipeline = pipeline[1]
        self.watchlist = {}

        self.queue = queue
        if queue:
            queue.add_thread(self)

        if self.FORCE_EVENT_UPDATE:
            # sets up SteelSeries events
            ss.unbind_all_event(self.GAME, self.EVENT_LIST, self.addr)
            for event_name, event in self.EVENT_LIST.items():
                ss.bind_oled_event(self.GAME, event_name, ss.create_oled_data(
                    event["lines"], event["bar"], event["icon"]), self.addr)

    # returns the access token
    def set_auth(self):
        try:
            with open("config\\twitch_token.oauth", "r") as token_file:
                auth = json.load(token_file)
                self.client_id = auth["client_id"]
                self.client_secret = auth["client_secret"]
                self.user_token = auth["user_access_token"]
                self.user_token_expiry = auth["user_token_expiry"]
                self.app_token = auth["app_access_token"]
                self.app_token_expiry = auth["app_token_expiry"]
                return True
        except Exception as e:
            log("Error: The OAuth file doesn't exist!", self)
            log(e, self)
            return False

    # returns authorization header used for every request
    def get_auth_header(self, token_type="app"):
        # token is about to expire, or already expired
        if self.user_token_expiry - time() <= 60:
            generate_token("twitch", "user")
            self.set_auth()
        elif self.app_token_expiry - time() <= 60:
            generate_token("twitch", "app")
            self.set_auth()

        if token_type == "app":
            return {
                "Client-ID": self.client_id,
                "Authorization": "Bearer "+str(self.app_token)
            }
        elif token_type == "user":
            return {
                "Client-ID": self.client_id,
                "Authorization": "Bearer "+str(self.user_token)
            }

    # fecthes the list of currently subscribed webhooks
    def get_webhooks_subs(self):
        params = {
            "first": 100
        }
        r = requests.get("https://api.twitch.tv/helix/webhooks/subscriptions",
                         headers=self.get_auth_header("app"), params=params)

        sub_list = []
        try:
            data = r.json()
            sub_list.extend(data["data"])
        # another error
        except:
            data = {}
            log("Error: "+str(r.status_code) +
                " while trying to retrieve webhook subs", self)

        # keep requesting data until out of pages
        while(data.get("pagination", {}).get("cursor", None)):
            params = {"first": 100, "after": data.get(
                "pagination", {}).get("cursor", None)}
            data = requests.get("https://api.twitch.tv/helix/webhooks/subscriptions",
                                headers=self.get_auth_header("app"), params=params).json()
            sub_list.extend(data["data"])

        return sub_list

    # unsubscribes and resubcribes to any webhooks that are about to expire
    def refresh_webhooks_subs(self):
        sub_list = self.get_webhooks_subs()
        now = dt.datetime.utcnow()  # current UTC time
        for sub in sub_list:
            exp_date = sub.get("expires_at", 0)
            # no time retrieve or less than 1h before expiration
            if not(exp_date) or (now - dt.datetime.strptime(exp_date, '%Y-%m-%dT%H:%M:%SZ')).total_seconds() < 3600:
                self.webhook_resub(sub)

    # crafts the proper URL assioated with a topic
    def get_topic(self, topic, user_id=None):
        if topic == "streams":
            return topic+"?user_id="+str(user_id)
        elif topic == "users/follows":
            return topic+"?first=1&to_id="+str(user_id)
        else:
            log("Topic "+topic+" not supported", self)
            return topic

    # retrieves the following list of a user
    def get_followings(self, username="rawb_"):
        params = {
            "from_id": self.get_user_id(username),
            "first": 100
        }
        r = requests.get("https://api.twitch.tv/helix/users/follows",
                         params=params, headers=self.get_auth_header("app"))

        following_list = []
        try:
            data = r.json()
            following_list.extend([follower.get("to_name", "")
                                  for follower in data.get("data", [])])
        # another error
        except:
            data = {}
            log("Error: "+str(r.status_code) +
                " while trying to retrieve followings", self)

        # keep requesting data until out of pages
        while(data.get("pagination", {}).get("cursor", None)):
            params.update({
                "after": data.get("pagination", {}).get("cursor", None)
            })
            data = requests.get("https://api.twitch.tv/helix/users/follows",
                                headers=self.get_auth_header("app"), params=params).json()
            following_list.extend([follower.get("to_name", "")
                                  for follower in data.get("data", [])])

        return following_list

    def generate_sublist_from_user(self, username="rawb_", topics=["streams"]):
        followings = self.get_followings(username)
        return {channel: {"topic": topics} for channel in followings}

    # subscribes to every topic assiciated to a channel
    def webhook_sub_topics(self, sublist):
        current_subs = self.get_webhooks_subs()
        # removes the "expires_at" key so that new subs and current_subs match for comparison
        for sub in current_subs:
            sub.pop("expires_at", None)
        for username in sublist.keys():
            user_id = self.get_user_id(username)
            if(user_id == None):
                continue
            for topic in sublist[username].get("topic", []):
                sub = {
                    "callback": "http://rawb.ddns.net:5555/twitch/"+topic+"/"+username,
                    "topic": "https://api.twitch.tv/helix/"+self.get_topic(topic, user_id=user_id)
                }
                if sub in current_subs:
                    log(username+" already subscribed with topic "+topic, self)
                else:
                    self.webhook_resub(sub)

    # resubscribes from a given webhook
    def webhook_resub(self, sub):
        self.webhook_unsub(sub)
        self.webhook_sub(sub)

    # subscribes to a webhook
    def webhook_sub(self, sub):
        self.webhook_sub_unsub(sub, "subscribe")

        channel = sub["callback"].split("/")[-1]
        r = requests.get("https://api.twitch.tv/helix/streams",
                         params={"user_login": channel}, headers=self.get_auth_header("app"))

        if not(200 <= r.status_code < 300):
            log("Error: "+str(r.status_code) +
                " while trying to get channels", self)

        try:
            data = r.json().get("data", [{}])[0]
            self.set_channel(self.watchlist, channel, data.get(
                "type", False), data.get("game_id", None))
        except IndexError as e:
            self.set_channel(self.watchlist, channel)
        except Exception as e:
            log("Error while trying to fetch channel status", self)
            log(e, self)
            return None

    # unsubscribes from a given webhook
    def webhook_unsub(self, sub):
        self.webhook_sub_unsub(sub, "unsubscribe")

    # unsubscribes from every subbed webhook
    def webhook_unsub_all(self):
        for sub in self.get_webhooks_subs():
            #print("unsubbing from: "+sub["topic"])
            self.webhook_unsub(sub)

    # subscribes or unsubscribes to a webhook
    def webhook_sub_unsub(self, sub, mode):
        params = {
            "hub.mode": mode,
            "hub.topic": sub["topic"],
            "hub.callback": sub["callback"],
            "hub.lease_seconds": 864000,
            "hub.secret": self.client_secret
        }
        r = requests.post("https://api.twitch.tv/helix/webhooks/hub",
                          params=params, headers=self.get_auth_header("app"))

        if not(200 <= r.status_code < 300):
            log("Error: "+str(r.status_code) +
                " while trying to "+mode+" to:", self)
            log(params, self)

    # get user ID from username
    def get_user_id(self, username):
        r = requests.get("https://api.twitch.tv/helix/users?login=" +
                         username, headers=self.get_auth_header("app"))

        if not(200 <= r.status_code < 300):
            log("Error: "+str(r.status_code) +
                " while trying to get username: "+username, self)

        try:
            return r.json()["data"][0]["id"]
        except:
            log("Error while trying to read username: "+username, self)
            return None

    # get user ID from username
    def get_game_name(self, game_id):
        # No game ID specified
        if not(game_id):
            return ""
        r = requests.get("https://api.twitch.tv/helix/games?id=" +
                         game_id, headers=self.get_auth_header("app"))

        if not(200 <= r.status_code < 300):
            log("Error: "+str(r.status_code) +
                " while trying to get game: "+game_id, self)

        try:
            return r.json()["data"][0]["name"]
        except:
            log("Error while trying to read get game: "+game_id, self)
            return None

    # get profile picture from username
    def get_profile_picture(self, channel):
        if not(channel):
            return "/static/images/notification/twitch.png"
        r = requests.get("https://api.twitch.tv/helix/users?login=" +
                         channel, headers=self.get_auth_header("app"))

        if not(200 <= r.status_code < 300):
            log("Error: "+str(r.status_code) +
                " while trying to get game: "+channel, self)

        try:
            return r.json()["data"][0]["profile_image_url"]
        except:
            log("Error while trying to read get game: "+channel, self)
            return "/static/images/notification/twitch.png"

    # sets the initial status of each subscribed channel
    def set_initial_channel_status(self):
        subs = self.get_webhooks_subs()
        channels = [sub["callback"].split("/")[-1] for sub in subs]
        watched_channels = {}

        # splits channels into batches of 100
        for i in range(int(len(channels)/100)+1):
            params = {
                "user_login": channels[100*i:100*(i+1)]
            }
            r = requests.get("https://api.twitch.tv/helix/streams",
                             params=params, headers=self.get_auth_header("app"))

            if not(200 <= r.status_code < 300):
                log("Error: "+str(r.status_code) +
                    " while trying to get channels", self)

            try:
                # adds channels to the watchlist
                for channel in r.json().get("data", []):
                    watched_channels.update({channel.get("user_name", "").lower(): {
                                            "is_live": channel.get("type", False), "game": channel.get("game_id", None)}})
            except Exception as e:
                log("Error while trying to while trying to get channels", self)
                log(e, self)
                return None

        return watched_channels

    # sets the status (live/offline) and the game of a channel
    def set_channel(self, watched_channels, channel, status=False, game_id=None):
        watched_channels.update(
            {channel: {"is_live": status, "game": game_id}})

    # reads the status of a channel
    def get_channel_status(self, watched_channels, channel):
        return watched_channels.get(channel, {}).get("is_live", False)

    # reads the game of a channel
    def get_channel_game(self, watched_channels, channel):
        return watched_channels.get(channel, {}).get("game", None)

    # channel was updated
    def channel_update(self, channel, raw_data):
        try:
            data = raw_data.get("data")[0]
        except:
            data = {}

        username = data.get("user_name", channel)
        status = data.get("type", False)
        game_id = data.get("game_id", None)
        game = self.get_game_name(game_id)
        title = data.get("title", False)
        now = dt.datetime.now()

        frame = {}

        # channel went offline
        if not(status):
            self.set_channel(self.watchlist, channel)
            log(username+" stopped streaming", self)
            # stream went offline: notifications about stream are removed
            if self.addr:
                self.pipeline.clear_duplicate(channel)
        # channel was already live
        elif self.get_channel_status(self.watchlist, channel):
            # game changed
            if game_id != self.get_channel_game(self.watchlist, channel):
                self.set_channel(self.watchlist, channel, status, game_id)
                frame = {
                    "channel": username,
                    "game": game,
                    "title": title
                }
            log(username+" was updated. Playing "+str(game), self)
            log(title, self)
        # channel went live
        else:
            self.set_channel(self.watchlist, channel, True, game_id)
            ## NOTIFY ##
            log(username+" just went live! Playing "+str(game), self)
            log(title, self)
            frame = {
                "channel": "LIVE: "+username,
                "game": game,
                "title": title
            }

        # only send an event if it's needed
        if frame:
            if self.addr:
                event_name = "STREAM"
                event = self.pipeline.craft_notification(
                    self.GAME, event_name, priority=self.EVENT_LIST[event_name]["priority"], blink_duration=1, duration=1, value=0, frame=frame, event_ID=channel)
                self.pipeline.add_event(event)

            if self.queue:
                notification = {
                    "type": "twitch",
                    "data": {
                            "username": username,
                        "game": game,
                        "title": title
                    },
                    "time": now.strftime("%H:%M"),
                    "date": now.strftime("%d/%m"),
                    "image_url": self.get_profile_picture(channel),
                    "link": "http://www.twitch.tv/"+username,
                    "color": "rgb(100,65,165)"
                }
                self.queue.put("notifications", "webserver", notification)

    # new follower notification
    def new_follower(self, username, raw_data):
        try:
            data = raw_data.get[0]
        except:
            data = {}
        follower = data.get("from_name", "")

        ## NOTIFIY ##
        log(follower.capitalize()+" now follows "+username.capitalize(), self)

    # receives keyboard events from KeyboardListener
    def keyboard_event(self, key):
        pass

    def run(self):
        log("Starting thread", self)
        if not(self.set_auth()):
            log("ERROR: Couldn't get token, exiting", self)
            exit()

        # self.refresh_webhooks_subs()
        self.webhook_sub_topics(self.SUB_LIST)
        self.watchlist = self.set_initial_channel_status()
        log(self.watchlist, self)
        # self.webhook_unsub_all()
        # self.webhook_sub_topics(self.generate_sublist_from_user())

        while(self.addr):
            # keep the keyboard communication alive
            ss.heartbeat(self.GAME, self.addr)
            sleep(0.5)
