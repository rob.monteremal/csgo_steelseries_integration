from src.utils.utils import log

import requests
import json
import webbrowser
from time import time

services_url = {
    "twitch": {
        "auth_url": "https://id.twitch.tv/oauth2/authorize",
        "token_url": "https://id.twitch.tv/oauth2/token",
        "refresh_url": "https://id.twitch.tv/oauth2/token"
    }
}

# Auth values
client_id = "fo8yv0e9z3b7d5gnas15tyf646u2at"
client_secret = "dab8xdeqemuhpo2w5yb2cfzmmnmpp8"
redirect_uri = "http://rawb.ddns.net:5555/twitch"
scope = "viewing_activity_read"

# returns the access token


def read_token(service, token_type="user", path="config\\"):
    try:
        with open(path+service+"_token.oauth", "r") as token_file:
            auth = json.load(token_file)
    except:
        log("Error: The OAuth file doesn't exist!")
        return None

    try:
        # No access token generated/saved
        if len(auth.get(token_type+"_access_token", "")) == 30:
            return auth.get(token_type+"_access_token", "")
        else:
            return None
    except:
        log("Error: OAuth file JSON structure incorrect!")
        return None

# generates either a new access token or refreshes the current one


def generate_token(service, token_type):
    if not(services_url.get(service, None)):
        log("Error: "+service+" is not recognized or supported!")
        return None

    # No token already generated/saved, or non-refreshable token
    if(not(read_token(service, token_type)) or token_type == "app"):
        log("No "+service+" "+token_type+" token found, generating new token")
        return generate_access_token(service, token_type)
    # Token exists but needs to be refreshed
    else:
        log(service.capitalize()+" "+token_type+" token found, refreshing token")
        return refresh_token(service, token_type)

# Generates a token pair of access and refresh token using OAuth authorization code flow


def generate_access_token(service, token_type="user"):
    if token_type == "user":
        return generate_user_access_token(service)
    elif token_type == "app":
        return generate_app_access_token(service)
    else:
        log("Unknown token type: "+token_type)
        return None


def generate_user_access_token(service, token_type="user", path="config\\"):
    try:
        with open(path+service+"_token.oauth", "r") as token_file:
            auth = json.load(token_file)
    except:
        log("Error: The OAuth file doesn't exist!")
        return None

    params = {
        "client_id": auth.get("client_id", ""),
        "client_secret": auth.get("client_secret", ""),
        "response_type": "code",
        "redirect_uri": auth.get("redirect_uri", ""),
        "scope": scope
    }

    auth_request = requests.get(
        services_url[service]["auth_url"], params=params)
    webbrowser.open(auth_request.url)

    # waits for the token to be written, is aborted after timeout
    t_start = time()
    while(time()-t_start < 10):
        # token has been successfully generated
        if(read_token(service)):
            return read_token(service)

    return None


def generate_app_access_token(service, token_type="app", path="config\\"):
    try:
        with open(path+service+"_token.oauth", "r") as token_file:
            auth = json.load(token_file)
    except:
        log("Error: The OAuth file doesn't exist!")
        return None

    params = {
        "client_id": auth.get("client_id", ""),
        "client_secret": auth.get("client_secret", ""),
        "grant_type": "client_credentials",
        "scope": scope
    }

    r = requests.post(services_url[service]["token_url"], params=params)

    if not(200 <= r.status_code < 300):
        log("Error generating "+service+" app access token!")
        log(r.status_code)
        return None

    data = r.json()
    auth.update({
        token_type+"_access_token": data.get("access_token", ""),
        token_type+"_refresh_token": data.get("refresh_token", ""),
        token_type+"_token_expiry": time() + data.get("expires_in", "")
    })

    with open(path+"twitch_token.oauth", "w") as file:
        json.dump(auth, file)

    return data["access_token"]

# refreshes the current access token using the refresh token


def refresh_token(service, token_type="user", path="config\\"):
    try:
        with open(path+service+"_token.oauth", "r") as token_file:
            auth = json.load(token_file)
    except:
        log("Error: The OAuth file doesn't exist!")
        return None

    params = {
        "client_id": auth.get("client_id", ""),
        "client_secret": auth.get("client_secret", ""),
        "grant_type": "refresh_token",
        "redirect_uri": auth.get("redirect_uri", ""),
        "refresh_token": auth.get(token_type+"_refresh_token", "")
    }

    refresh_request = requests.post(
        services_url[service]["refresh_url"], params=params)

    if not(200 <= refresh_request.status_code < 300):
        log("Error refreshing "+service+" token!")
        log(refresh_request.status_code)
        return None

    data = refresh_request.json()
    auth.update({
        token_type+"_access_token": data.get("access_token", ""),
        token_type+"_refresh_token": data.get("refresh_token", ""),
        token_type+"_token_expiry": time() + data.get("expires_in", "")
    })

    try:
        with open(path+"twitch_token.oauth", "w") as file:
            json.dump(auth, file)
        return data["access_token"]

    except:
        log("Error: The OAuth file doesn't exist!")
        return None

# Responds to a auth request


def answer_auth(service, auth_code, token_type="user", path="config\\"):
    try:
        with open(path+service+"_token.oauth", "r") as token_file:
            auth = json.load(token_file)
    except:
        log("Error: The OAuth file doesn't exist!")
        return None

    params = {
        "client_id": auth.get("client_id", ""),
        "client_secret": auth.get("client_secret", ""),
        "code": auth_code,
        "grant_type": "authorization_code",
        "redirect_uri": auth.get("redirect_uri", "")
    }
    r = requests.post(services_url[service]["token_url"], params=params)

    # Twitch replies with an error code, no token received
    if not(200 <= r.status_code < 300):
        log("Error "+str(r.status_code) +
            " while trying to generate token for "+service)
        return None

    try:
        data = r.json()
        auth.update({
            token_type+"_access_token": data.get("access_token", ""),
            token_type+"_refresh_token": data.get("refresh_token", ""),
            token_type+"_token_expiry": time() + data.get("expires_in", "")
        })

        with open(path+service+"_token.oauth", "w") as file:
            json.dump(auth, file)

        return True

    except Exception as e:
        log(e)
        return None
