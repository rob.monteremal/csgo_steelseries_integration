import threading
from time import sleep
from src.utils.utils import log
from src.Modules.Background.weather import Weather
from src.Modules.Background.ping import Ping
from src.Modules.Background.clock import Clock
import src.SteelSeries.steelseries as ss
from src.SteelSeries.pipeline import EventPipeline
import sys
sys.path.append("..\\..")


class Background(threading.Thread):
    # Change to True if you modified any event in the event list
    FORCE_EVENT_UPDATE = False

    GAME = "BACKGROUND"
    EVENT_LIST = {
        "PING": {
            "lines": [],
            "bar": False,
            "icon": 0,
            "priority": 15
        },
        "DATETIME": {
            "lines": ["datetime", "weather"],
            "bar": False,
            "icon": 0,
            "priority": 115
        }
    }

    def __init__(self, pipeline=(0, None), queue=None):
        threading.Thread.__init__(self)
        self.addr = pipeline[0]
        self.pipeline = pipeline[1]

        if self.FORCE_EVENT_UPDATE:
            # sets up SteelSeries events
            ss.unbind_all_event(self.GAME, self.EVENT_LIST, self.addr)
            event = self.EVENT_LIST["DATETIME"]
            ss.bind_oled_event(self.GAME, "DATETIME", ss.create_oled_data(
                event["lines"], event["bar"], event["icon"]), self.addr)
            ss.bind_oled_event(self.GAME, "PING",
                               ss.create_bitmap_oled_data(), self.addr)

        self.weather_thread = Weather()
        self.ping_thread = Ping()
        self.clock_thread = Clock()

        self.show = "DATETIME"

    # receives keyboard events from KeyboardListener
    def keyboard_event(self, key):
        if key == "defil":
            self.toggleOutput()

    # changes between ping and datetime
    def toggleOutput(self):
        if self.show == "PING":
            self.show = "DATETIME"
            self.pipeline.clear_duplicate("PING")
        elif self.show == "DATETIME":
            self.show = "PING"
            self.pipeline.clear_duplicate("DATETIME")

    def run(self):
        log("Starting thread", self)
        self.weather_thread.start()
        self.clock_thread.start()
        self.ping_thread.start()

        while(True):
            ping = self.ping_thread.output
            weather = self.weather_thread.output
            datetime = self.clock_thread.output

            # some data couldn't be recovered yet
            if not(ping) or not(weather) or not(datetime):
                frame = {}
            else:
                frame = {
                    "PING": {"image-data-128x40": ping},
                    "DATETIME": {"datetime": datetime.get("date", "")+" "+datetime.get("time", ""), "weather": weather.get("current", "")+" | "+weather.get("tomorrow", "")}
                }

            event_name = self.show
            if frame:
                event = self.pipeline.craft_event(
                    self.GAME, event_name, self.EVENT_LIST[event_name]["priority"], 3, frame=frame.get(event_name, {}))
                self.pipeline.add_event(event)

            # keep the keyboard communication alive
            ss.heartbeat(self.GAME, self.addr)

            sleep(0.5)
