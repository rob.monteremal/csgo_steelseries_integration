import threading
import requests
from time import sleep
from datetime import datetime as dt
import json
from src.utils.utils import log
import sys
sys.path.append("..\\..")


class Weather(threading.Thread):

    def __init__(self, queue=None):
        threading.Thread.__init__(self)

        self.queue = queue
        self.output = None
        if queue:
            queue.add_thread(self)

    # sets API key and location information
    def set_info(self):
        try:
            with open("config\\openweathermap_api.info", "r") as info_file:
                info = json.load(info_file)
                self.api_key = info["api_key"]
                self.lat = info["lat"]
                self.long = info["long"]
                return True
        except Exception as e:
            log("Error: The openweathermap file cannot be opened", self)
            log(e, self)
            return False

    # retrieves the weather data of the set location (lat, long)
    # using openweathermap One Call API
    def get_weather_data(self):
        params = {
            "lat": self.lat,
            "lon": self.long,
            "appid": self.api_key,
            "units": "metric",
            "exclude": ["minutely", "alerts"]
        }
        r = requests.get(
            "https://api.openweathermap.org/data/2.5/onecall", params=params)

        try:
            data = r.json()

            if self.queue:
                default_temps = {
                    "morn": 0,
                    "day": 0,
                    "eve": 0,
                    "night": 0,
                    "min": 0,
                    "max": 0
                }
                days = []
                for day in data["daily"][self.get_tomorrow_index():self.get_tomorrow_index()+4]:
                    day_data = {
                        "date": dt.fromtimestamp(day.get("dt", dt.timestamp(dt.now()))).strftime("%d/%m"),
                        "temp": day.get("temp", default_temps),
                        "rain_prob": day.get("pop", 0),
                        "weather_icon": self.get_icon(day.get("weather", [{}])[0].get("icon", "10d")),
                        "weather": day.get("weather", [{}])[0].get("description", "Night")
                    }
                    days.append(day_data)
                hours = []
                for i in self.get_hours_index():
                    hour = data["hourly"][i]
                    hour_data = {
                        "hour": dt.fromtimestamp(hour.get("dt", dt.timestamp(dt.now()))).strftime("%#H"),
                        "temp": hour.get("temp", 0),
                        "rain_prob": hour.get("pop", 0),
                        "weather_icon": self.get_icon(hour.get("weather", [{}])[0].get("icon", "10d")),
                        "weather": hour.get("weather", [{}])[0].get("description", "Night")
                    }
                    hours.append(hour_data)

                weather = {
                    "hours": hours,
                    "days": days
                }

                self.queue.put("weather", "webserver", weather)

        except Exception as e:
            log("Error: "+str(r.status_code) +
                " while trying to retrieve weather data", self)
            log(e)

    def get_icon(self, icon):
        return "http://openweathermap.org/img/wn/"+icon+"@2x.png"

    # returns the daily forecast index representing "tomorrow"
    def get_tomorrow_index(self):
        # current time between 5am and midnight
        if int(dt.now().strftime("%H")) > 5:
            return 1
        else:
            return 0

    # returns the list of indices for the hourly forecast
    def get_hours_index(self):
        current_hour = int(dt.now().strftime("%H"))
        hours = []
        new_offset = None
        for offset in [0, 3, 6, 9]:
            # no forecast before 0700
            if current_hour < 7:
                hour = 7 + offset - current_hour
            # forecast after 1800 gives forecast for next day starting at 0700
            elif current_hour + offset > 18:
                if new_offset is None:
                    hour = 24 + 7 - current_hour
                    new_offset = -offset
                else:
                    hour = 24 + 7 - current_hour + offset + new_offset
            else:
                hour = offset
            hours.append(hour)

        return hours

    # returns conditions text depending on ID
    def get_conditions(self, weather_id):
        if 200 <= weather_id < 300:
            return "Thunder"
        elif 300 <= weather_id < 400:
            return "L. rain"
        elif 500 <= weather_id < 600:
            return "Rain"
        elif 600 <= weather_id < 700:
            return "Snow"
        elif 800 <= weather_id < 802:
            return "Clear"
        elif 802 <= weather_id < 804:
            return "Cloudy"

    def keyboard_event(self, key):
        pass

    def queue_event(self, event):
        pass

    def run(self):
        if not(self.set_info()):
            return None
        log("Starting thread", self)

        while(True):
            self.get_weather_data()

            # prevent openweathermap API spam
            sleep(600)
