import threading
from time import sleep
from datetime import datetime
from src.utils.utils import log
import sys
sys.path.append("..\\..")


class Clock(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)

    # returns date&time as a proper event
    def get_time(self):
        dt = datetime.now()
        return {"time": dt.strftime("%H:%M:%S"), "date": dt.strftime("%a, %b %d")}

    # constantly checks the date&time
    def run(self):
        log("Starting thread", self)
        while True:
            self.output = self.get_time()
