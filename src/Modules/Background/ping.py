import threading
from collections import deque
from time import sleep, time
import re
from subprocess import Popen, PIPE
from socket import getaddrinfo
from src.utils.utils import log
import src.SteelSeries.bitmap as bm
import sys
sys.path.append("..\\..")


class Ping(threading.Thread):
    cont = True
    hidden = False
    MAX_PING = 1000

    def __init__(self):
        threading.Thread.__init__(self)

        self.output = None

    # sets the ping target IP address (to avoid doing a DNS lookup each time)
    def set_addr(self, URL="google.fr"):
        try:
            self.ping_addr = str(getaddrinfo(URL, 80)[0][4][0])
            self.timeout_count = 0
            log("Ping address: %s" % (self.ping_addr), self)
        except:
            log("getaddrinfo error!", self)

    # takes a ping result and outputs the actual ping value in ms
    def get_ping_time(self, res_str):
        try:
            ping_value = int(re.compile(
                "[0-9]+ms").findall(str(res_str))[0][:-2])
            self.timeout_count = 0
            return ping_value
        except:
            self.timeout_count += 1
            return self.MAX_PING-1

    # runs ping command and retrieves result
    def ping(self):
        return self.get_ping_time(Popen(["ping.exe", self.ping_addr, "-n", "1", "-w", str(self.MAX_PING)], stdout=PIPE).communicate()[0])

    def avg(self):
        try:
            return sum(self.points) / len(self.points)
        except Exception as e:
            log(e, self)
            return -1

    # adds the new ping value to the graph queue
    def add_to_stack(self, ping_value):
        self.points.append(ping_value)
        # only store latest 128 pings
        # safety: shouldn't run more than once per call
        while(len(self.points) > 128):
            self.points.pop(0)

    # stops the thread
    def stop(self):
        self.cont = False

    # stops the thread
    def resume(self):
        self.cont = True

    def run(self):
        log("Starting thread", self)
        self.set_addr()
        self.points = []
        self.g_points = []
        fetch_rate = self.MAX_PING / 1000
        background = bm.image_to_bitmap(
            "Bitmap_reference\\ping_background.png")

        while(True):
            while(self.cont):
                t_start = time()
                ping_time = self.ping()
                self.add_to_stack(ping_time)

                # Bitmap
                bitmap = bm.ping_values_to_bitmap(
                    background.copy(), self.avg(), ping_time)
                _, self.g_points = bm.insert_graph_in_bitmap(
                    bitmap, self.points, self.g_points)

                self.output = bm.bitmap_to_steelseries(bitmap).copy()

                # Too many timeouts lead to reset (interface might have changed)
                if self.timeout_count >= 3:
                    self.set_addr()

                sleep_time = fetch_rate - time() + t_start
                if sleep_time < 0:
                    sleep_time = 0
                sleep(sleep_time)
