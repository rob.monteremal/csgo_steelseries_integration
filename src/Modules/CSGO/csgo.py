# other project files
import sys
sys.path.append("..\\..")
from src.SteelSeries.pipeline import EventPipeline
import src.SteelSeries.steelseries as ss
from src.utils.utils import log

import json
import requests

# Logger
import logging
debug = False
logger = logging.getLogger("werkzeug")
logger.setLevel(logging.ERROR)
logger.disabled = debug

# multithreading
import threading

class CSGO(threading.Thread):
	# Change to True if you modified any event in the event list
	FORCE_EVENT_UPDATE = False

	GAME = 'CSGO_CUSTOM'

	primary_colors = {
		"white":	{"red": 255, "green":255, "blue":255},
		"black":	{"red": 0, "green":0, "blue":0},
		"red":		{"red": 255, "green":0, "blue":0},
		"green":	{"red": 0, "green":255, "blue":0},
		"blue":		{"red": 0, "green":0, "blue":255}
		}

	weapon_binds_t = {
		# Utility
		106: {"price": 650, "items": ["vest"], "in_inventory": False},
		107: {"price": 1000, "items": ["vesthelm"], "in_inventory": False},
		# Weapons
		84: {"price": 300, "items": ["p250"], "in_inventory": False},
		85: {"price": 500, "items": ["tec9", "fiveseven", "cz75a"], "in_inventory": False},
		86: {"price": 700, "items": ["deagle", "revolver"], "in_inventory": False},
		62: {"price": 2700, "items": ["ak47", "m4a1", "m4a1_silencer"], "in_inventory": False},
		63: {"price": 4750, "items": ["awp"], "in_inventory": False},
		64: {"price": 1700, "items": ["ssg08"], "in_inventory": False},
		65: {"price": 1200, "items": ["ump45"], "in_inventory": False},
		# Grenades
		6: {"price": 300, "items": ["smokegrenade"], "in_inventory": False},
		7: {"price": 200, "items": ["flashbang"], "in_inventory": False},
		8: {"price": 300, "items": ["hegrenade"], "in_inventory": False},
		9: {"price": 400, "items": ["molotov", "incgrenade"], "in_inventory": False},
		10: {"price": 1200, "items": ["nades_full"], "in_inventory": False},
		11: {"price": 1000, "items": ["nades_awp"], "in_inventory": False},
		12: {"price": 800, "items": ["nades_low"], "in_inventory": False}
		}

	weapon_binds_ct = {
		# Utility
		106: {"price": 650, "items": ["vest"], "in_inventory": False},
		107: {"price": 1000, "items": ["vesthelm"], "in_inventory": False},
		108: {"price": 400, "items": ["defuser"], "in_inventory": False},
		# Weapons
		84: {"price": 300, "items": ["p250"], "in_inventory": False},
		85: {"price": 500, "items": ["tec9", "fiveseven", "cz75a"], "in_inventory": False},
		86: {"price": 700, "items": ["deagle", "revolver"], "in_inventory": False},
		62: {"price": 3100, "items": ["ak47", "m4a1", "m4a1_silencer"], "in_inventory": False},
		63: {"price": 4750, "items": ["awp"], "in_inventory": False},
		64: {"price": 1700, "items": ["ssg08"], "in_inventory": False},
		65: {"price": 1200, "items": ["ump45"], "in_inventory": False},
		# Grenades
		6: {"price": 300, "items": ["smokegrenade"], "in_inventory": False},
		7: {"price": 200, "items": ["flashbang"], "in_inventory": False},
		8: {"price": 300, "items": ["hegrenade"], "in_inventory": False},
		9: {"price": 600, "items": ["molotov", "incgrenade"], "in_inventory": False},
		10: {"price": 1400, "items": ["nades_full"], "in_inventory": False},
		11: {"price": 1000, "items": ["nades_awp"], "in_inventory": False},
		12: {"price": 800, "items": ["nades_low"], "in_inventory": False}
		}

	# constructor
	def __init__(self, app, pipeline=(0, None), queue=None):
		threading.Thread.__init__(self)
		self.addr = pipeline[0]
		self.pipeline = pipeline[1]

		if self.FORCE_EVENT_UPDATE:
			# sets up SteelSeries events
			ss.unbind_all_event(self.GAME, self.EVENT_LIST, self.addr)
			ss.bind_all_event(self, self.EVENT_LIST, self.addr)
			ss.bind_bitmap_event(self.GAME, "BITMAP", self.addr)

		# init values for breathing effect
		self.count = 0
		self.direction = 1
		self.first_launch = True	# debug

		# CSGO server config
		self.host = "127.0.0.1"
		self.port = 22222
		self.app = app

	# creates and binds the event related to getting flashed
	def flash(self, event_name):
		colors = {
			"gradient": {
				"zero": self.primary_colors["black"],
				"hundred": self.primary_colors["white"]
			}
		}

		return ss.create_bind(self.GAME, event_name, 0, 100, ss.create_handlers_all_devices(self.GAME, colors, {}, {}))

	# creates and binds the event related to the bomb (planted, exploded, defused)
	def bomb(self, event_name):
		colors = 	[
			{				# Bomb planted - slow flash RED
				"low": 0,
				"high": 0,
				"color": self.primary_colors["red"]
			},			{	# Bomb exploded - fast flash RED
				"low": 1,
				"high": 1,
				"color": self.primary_colors["red"]
			},			{	# Bomb defused - fast flash BLUE
				"low": 2,
				"high": 2,
				"color": self.primary_colors["blue"]
			}
		]
		frequencies =		[
			{
				"low": 0,
				"high": 0,
				"frequency": 1
			},			{
				"low": 1,
				"high": 1,
				"frequency": 10
			},			{
				"low": 2,
				"high": 2,
				"frequency": 10
			}
		]
		repeat_limits =		[
			{
				"low": 0,
				"high": 0,
				"repeat_limit": 45	# failsafe - bomb duration shoudn't exceed 40 sec
			},			{
				"low": 1,
				"high": 1,
				"repeat_limit": 20
			},			{
				"low": 2,
				"high": 2,
				"repeat_limit": 20
			}
		]
		
		return ss.create_bind(self.GAME, event_name, 0, 2, ss.create_handlers_all_devices(self.GAME, colors, frequencies, repeat_limits))

	# computes the list of colors associated with bind buy keys
	# red: cannot buy (not enough money)
	# orange: already in inventory
	# green: can be bought
	def buy_binds(self, team, money):
		if (team == "CT"):
			keys = self.weapon_binds_ct
		elif (team == "T"):
			keys = self.weapon_binds_t
		else:
			return []

		colors = {}
		for key in keys:
			if keys[key]["in_inventory"]:
				colors.update({key: {"red": 255, "green": 105, "blue": 0}})
			elif money >= keys[key]["price"]:
				colors.update({key: self.primary_colors["green"]})
			else:
				colors.update({key: self.primary_colors["red"]})

		return colors

	# updates the currently held items 
	def update_inventory(self, team, equipement, armor, helmet, kit):
		if (team == "CT"):
			keys = self.weapon_binds_ct
		else:
			keys = self.weapon_binds_t	

		for key in keys:
			update = False
			for item in keys[key]["items"]:
				if item in equipement:
					update = True
					break
			keys[key]["in_inventory"] = update

		# utility
		keys[107]["in_inventory"] = helmet
		keys[106]["in_inventory"] = bool(armor)
		if team == "CT":
			keys[108]["in_inventory"] = kit

	# retrieves currently held items 
	def get_equipement(self, weapons):
		equipement = []
		for weapon in weapons:
			equipement.append( weapons[weapon]["name"].split('_')[1] )

		return equipement

	def team_color(self, event_name, team, value):
		min = 0
		max = 100

		if team == "CT":
			color_start = {"red": 0, "green": 0, "blue": 120}
			color_end = {"red": 115, "green": 115, "blue": 255}
		else:
			color_start = {"red": 180, "green": 165, "blue": 10}
			color_end = {"red": 105, "green": 85, "blue": 30}

		return ss.color_breathe(value, color_start, color_end, min, max)

	def match_info(self, team, match_data):
		map = match_data.get("name", "")

		if team == "CT":
			score_player = match_data.get("team_ct", {}).get("score", 0)
			score_enemy = match_data.get("team_t", {}).get("score", 0)
		elif team == "T":
			score_player = match_data.get("team_t", {}).get("score", 0)
			score_enemy = match_data.get("team_ct", {}).get("score", 0)

		if score_player > score_enemy:
			state = "WINNING"
		elif score_player < score_enemy:
			state = "LOSING"
		else:
			state = "TIED"

		log("%d - %d | %s | %s" %(score_player, score_enemy, state, map), self)

		return {"map": map, "score_player": score_player, "score_enemy": score_enemy, "state": state}

	# triggered each time a new HTTP request is received
	def incoming_data(self, request):
		try:
			data = request.get_json()
		except:
			data = {}
			log("Error: "+str(request.status_code), self)

		background = False
		prev = data.get("previously", {})
		
		# ugly JSON unpacking
		player = data.get('player', {}).get('state', {})
		round = data.get('round', {})
		team = data.get('player', {}).get('team', "")
		money = player.get('money', 0)
		is_flashed = player.get('flashed', 0)
		phase = round.get('phase', "")
		bomb_state = round.get('bomb', "")
		#health = player['health']
		
		response = ""
		# only the first time the program is open (REMOVE BEFORE RELEASE/ONLY FOR DEBUG)
		if self.first_launch:
			self.update_inventory(team, self.get_equipement( data.get("player", {}).get("weapons", {}) ), player.get("armor", 0), player.get("helmet", False), player.get("defusekit", False) )
			self.first_launch = False

		### FULL KEYBOARD BACKGROUND EFFECTS
		# Player is flashed - show white
		if is_flashed:
			background = True
			bitmap = ss.full_bitmap(ss.color_breathe(is_flashed, self.primary_colors["black"], self.primary_colors["white"], 0, 255))
		# Bomb JUST planted
		elif data.get("added", {}).get("round", {}).get("bomb", False) and bomb_state == "planted":
			response += "BOMB PLANTED "
			ss.send_steelseries(ss.game_event(self.GAME, "BOMB", 0), self.addr, "game_event")
		# Bomb explosion
		elif bomb_state == "exploded" and prev.get("round", {}).get("bomb", "") == "planted":
			response += "BOMB JUST EXPLODED "
			ss.send_steelseries(ss.game_event(self.GAME, "BOMB", 1), self.addr, "game_event")
		# Bomb defused
		elif bomb_state == "defused" and prev.get("round", {}).get("bomb", "") == "planted":
			response += "BOMB DEFUSED "
			ss.send_steelseries(ss.game_event(self.GAME, "BOMB", 2), self.addr, "game_event")
		# Bomb planted
		elif bomb_state == "planted":
			# prevents timeout
			ss.heartbeat(self.GAME, self.addr)
		# nothing is happening (but playing)
		elif team != "" and bomb_state == "" and phase != "over":
			background = True
			self.count += self.direction
			# swap direction
			if self.count >= 100 or self.count <= 0:
				self.direction = -self.direction
			bitmap = ss.full_bitmap(self.team_color("BITMAP", team, self.count))
		# not currently in a match
		else:
			pass

		### KEYS OVERLAYED
		# Buy time - show buy binds corresponding to the current team
		if phase == "freezetime":
			# Round just ended, reload equipement picked up during the round
			if prev.get("round", {}).get("phase", "") == "over":
				self.update_inventory(team, self.get_equipement( data.get("player", {}).get("weapons", {}) ), player.get("armor", 0), player.get("helmet", False), player.get("defusekit", False) )
			# equipement value changed (new items/dropped items)
			if prev.get("player", {}).get("state", {}).get("equip_value", 0):
				response += "New equipement "
				self.update_inventory(team, self.get_equipement( data.get("player", {}).get("weapons", {}) ), player.get("armor", 0), player.get("helmet", False), player.get("defusekit", False) )
			if background:
				bitmap = ss.insert_in_bitmap(bitmap, self.buy_binds(team, money))

		### OLED SCREEN
		if (prev.get("map", {}).get("round", -1) != -1):
			self.match_info(team, data.get("map", {}))

		if background:
			ss.send_steelseries(ss.create_bitmap_event(self.GAME, "BITMAP", bitmap, []), self.addr, "game_event")

		# prints to Python console, only used when debugging
		if len(response) > 0 and debug:
			log(response, self)

		return response

	EVENT_LIST = {	
		"BOMB": bomb
		}

    # receives keyboard events from KeyboardListener
	def keyboard_event(self, key):
		pass
	
	def run(self):
		log("Starting thread", self)
		log("CS:GO listener server running on %s:%s" %(self.host, self.port), self)
		self.app.run(host=self.host, port=self.port)


if __name__ == "__main__":
	addr = ss.get_addr()