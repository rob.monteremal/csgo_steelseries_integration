import src.SteelSeries.steelseries as ss # pylint: disable=no-name-in-module,import-error
from src.utils.utils import log

from time import time, sleep

# multithreading
import threading

# Pipeline thread execution wrapper
class EventPipelineThread(threading.Thread):

	def __init__(self, addr, tickrate):
		threading.Thread.__init__(self)
		self.pipeline = EventPipeline(addr, tickrate)

	# returns the actual pipeline
	def get_pipeline(self):
		return self.pipeline

    # receives keyboard events from KeyboardListener
	def keyboard_event(self, key):
		self.pipeline.keyboard_event(key)

	# runs the pipeline on a clock
	def run(self):
		log("Starting thread", self)
		while(self.pipeline.cont):
			self.pipeline.tick()
			sleep(self.pipeline.tickrate)

# Stores events and their metadata to execute them properly
class EventPipeline():
	
	def __init__(self, addr, tickrate):
		self.events = []
		self.addr = addr
		self.tickrate = tickrate
		self.cont = True

	# crafts an event to be added to the pipeline
	def craft_event(self, game, event_name, priority=10, duration=1, value=0, frame={}, event_ID=None):
		# default ID is the name
		if event_ID == None:
			event_ID = event_name

		event = {
			"GAME": game,
			"EVENT_NAME": event_name,
			"priority": priority,
			"value": value,
			"frame": frame,
			"time_end": time() + duration,
			"event_ID": event_ID
		}

		return event

	# crafts a notification event to be added to the pipeline
	# notification events have lower default priority and blink on&off
	def craft_notification(self, game, event_name, priority=3, blink_duration=1, duration=1, value=0, frame={}, event_ID=None):
		event = self.craft_event(game, event_name, priority, duration=blink_duration, value=value, frame=frame, event_ID=event_ID)
		event.update({
			"blink_duration": blink_duration,
			"show_event": True
		})
		return event

	# adds event to pipeline
	# events are ordered by priority
	def add_event(self, new_event):
		self.clear_duplicate(new_event["event_ID"])
		# events are ordered by priority
		for idx, event in enumerate(self.events):
			if new_event["priority"] <= event["priority"]:
				return self.events.insert(idx, new_event)
		return self.events.append(new_event)

	# removes duplicates of an event
	def clear_duplicate(self, event_ID):
		for event in self.events[:]:
			if event["event_ID"] == event_ID:
				self.events.remove(event)

	# returns current event aka the event at the top of the stack that's shown
	def get_current_event(self):
		try:
			# return the first event if it's a notification (and is shown)
			if self.events[0].get("show_event", True):
				return self.events[0]
			# or return the first non notification event
			for event in self.events[1:]:
				if not(event.get("blink_duration", 0)):
					return event
			return None
		except:
			return None

	# returns the current notification event
	def get_current_notification(self):
		try:
			for event in self.events:
				if event.get("blink_duration", False):
					return event
			return None
		except:
			return None

	# removes the current notification
	def clear_notification(self):
		try:
			self.events.remove(self.get_current_notification())
		except:
			pass

	# prints the current pipeline (game, event_name and show_event) for each event
	def print_pipeline(self):
		log([[event["GAME"], event["EVENT_NAME"], event.get("show_event", True)] for event in self.events], self)

	# sends steelseries data to exececute an event
	def exec_event(self, event):
		if event != None:
			#log("Pipeline size: %d" %(len(self.events)), self)
			#log("Executing event %s until %s" %(event["EVENT_NAME"], str(event["time_end"]-time())), self)
			ss.send_steelseries(ss.game_event(event["GAME"], event["EVENT_NAME"], event["value"], event["frame"]), self.addr, "game_event")

	# removes expired events and updates current one
	def tick(self):
		# self.print_pipeline()
		for event in self.events[:]:
			# event expired
			if time() > event["time_end"]:
				# notification event: gets refreshed and shown/hidden
				if event.get("blink_duration", 0):
					event.update({
						"time_end": time() + event["blink_duration"],
						"show_event": not(event["show_event"])
					})
				# regular event: gets deleted
				else:
					self.events.remove(event)
		self.exec_event(self.get_current_event())

	# stops the clock running the pipeline
	def stop(self):
		self.cont = False

    # receives keyboard events from KeyboardListener
	def keyboard_event(self, key):
		if key == "pause":
			self.clear_notification()
