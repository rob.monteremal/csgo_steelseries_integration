from src.utils.utils import log

import json
import requests

log_file = "C:/Users/Rob/Desktop/toto.log"

# Dumps a JSON to a file
def log_to_file(data):
	with open(log_file, 'w') as file:
		json.dump(data, file)

# Reads the SteelSeries config file and returns the port of the listener
def get_addr():
	with open("C:/ProgramData/SteelSeries/SteelSeries Engine 3/coreProps.json") as file:
		addr = json.load(file)['address']
		
	return addr

# crafts proper SteelSeries game event JSON POST request for a value
def game_event(GAME, event_name, value=0, frame={}):
	return {'game': GAME, 'event': event_name, 'data': {'value': value, 'frame': frame} }

# crafts proper SteelSeries game event JSON POST request for a frame
def game_event_frame(GAME, event_name, frame):
	return {'game': GAME, 'event': event_name, 'data': {'frame': frame} }

# (NEEDS SOFTWARE RESTART) sets the event timeout and game metadata:
# when no event or heartbeat have been receive for timeout_ms duration
# peripherals go back to their default lighting state
def set_timeout(GAME, game_name, timeout_ms, addr):
	send_steelseries( {'game': GAME, 'game_display_name':game_name, 'developer': "Rawb", 'deinitialize_timer_length_ms': timeout_ms}, addr, "game_metadata")

# keeps the events from timing out
def heartbeat(GAME, addr):
	send_steelseries({'game': GAME}, addr, "game_heartbeat")
	
# Sends JSON to SteelSeries listener
def send_steelseries(json, addr, endpoint):
	#log(json)
	headers = {'Content-type': 'application/json'}
	url = "http://"+str(addr)+"/"+str(endpoint)
	try:
		res = requests.post(url, json=json, headers=headers)
		if not(res.ok):
			log(res.json())
	except requests.exceptions.ConnectionError:
		log("Connection error with %s while sending %s" %(endpoint, json))

# removes every event in EVENT_LIST
def unbind_all_event(GAME, EVENT_LIST, addr):
	for event_name in EVENT_LIST:
		unbind_event(GAME, event_name, addr)

# removes previously bound event
def unbind_event(GAME, event_name, addr):
	send_steelseries( {'game':GAME,'event':event_name}, addr, "remove_game_event")
	
# creates and binds every event in EVENT_LIST
def bind_all_event(obj, EVENT_LIST, addr):
	for event_name in EVENT_LIST:
		bind_event(obj, EVENT_LIST, event_name, addr)

# sends command to bind an event
def bind_event(obj, EVENT_LIST, event_name, addr):
	send_steelseries(obj.EVENT_LIST[event_name](obj, event_name), addr, "bind_game_event")

# creates JSON to bind an event
def create_bind(GAME, event_name, min_value, max_value, handlers, value_opt=False):
	return {'game':GAME,'event':event_name,'min_value':min_value,'max_value':max_value,'handlers':handlers,'value_optional':value_opt}

# creates a dictionnary associating every targeted zone for each device
def create_devices_zones(GAME, devices, zones):
	devices_zones = {}
	for device in devices:
		devices_zones.update( {device: [ zone for zone in zones[:devices[device]] ] } )
	
	return devices_zones
	
# creates a handler for every device/zone combo
def create_handlers(GAME, devices_zones, colors, frequencies, repeat_limits):
	handlers = []
	for device in devices_zones:
		for zone in devices_zones[device]:
			handlers.append( {"device-type":device,"zone":zone,"mode":"color","color":colors,"rate":{"frequency":frequencies, "repeat_limit":repeat_limits} } )

	return handlers

# creates a handler for every zone on every supported device
def create_handlers_all_devices(GAME, colors, frequencies, repeat_limits):
	# mouse and mousepad
	devices = {"rgb-2-zone": 2, "rgb-8-zone": 8}
	zones = ["one", "two", "three", "four", "five", "six", "seven", "eight"]
	devices_zones = create_devices_zones(GAME, devices, zones)
	
	# keyboard
	devices_zones.update( {"rgb-per-key-zones": ["all"]} )
	
	return create_handlers(GAME, devices_zones, colors, frequencies, repeat_limits)

# binds bitmap event
def bind_bitmap_event(GAME, event_name, addr):
	send_steelseries({"game":GAME,"event":event_name,"value_optional":True,"handlers":[{"device-type":"rgb-per-key-zones","mode":"partial-bitmap"}]}, addr, "bind_game_event")

# returns a bitmap array of a single color
def full_bitmap(color):
	return [color for i in range(132)]

# creates bitmap event excluding excl events
def create_bitmap_event(GAME, event_name, bitmap, excl):
	#log_to_file({'game':GAME,'event':event_name,'data': {'frame': {'bitmap':full_bitmap(color), 'excluded-events':excl} } })
	return {'game':GAME,'event':event_name,'data': {'frame': {'bitmap':bitmap, 'excluded-events':excl} } }

# breathing effect between 2 values associated with 2 colors
def color_breathe(value, color_start, color_end, min_value, max_value):
	color = []
	for col in ["red", "green", "blue"]:
		color.append( int( (color_end[col] - color_start[col]) / (max_value - min_value) * value) + color_start[col])

	return color

# inserts a dictionnary of key:color replacing keys in already defined bitmap
def insert_in_bitmap(bitmap, colors):
	for key in colors:
		bitmap[key] = [ colors[key]["red"], colors[key]["green"], colors[key]["blue"] ]

	return bitmap

# binds OLED screen events
def bind_oled_event(GAME, event_name, datas, addr):
	send_steelseries({"game": GAME, "event":event_name, "value_optional":True, "handlers":[{"device-type":"screened","mode":"screen","zone":"one","datas":datas }] }, addr, "bind_game_event")

# returns proper OLED dataframe 
def create_oled_data(frame_list, progress_bar=False, icon=0):
	lines = [{"has-text": True, "context-frame-key": frame} for frame in frame_list]
	if progress_bar:
		lines.append({"has-progress-bar": True})
	return [{"lines": lines, "icon-id": icon}]

# 
def create_bitmap_oled_data(image_data=None):
	if image_data == None:
		image_data = [0 for i in range(640)]
	return [{"has-text": False, "image-data": image_data}]

# Shifts text by a number of words
def text_wrap_word(line, shift):
	words = line.split(" ")
	words_ = words[shift:]
	for word in words[:shift]:
		words_.append(word)
	return ' '.join(word for word in words_)

# Shifts every line by a number of words and updates the shift value
def text_wrap_word_all(lines):
	for line in lines:
		lines[line]["shift"] = (lines[line]["shift"] + 1)%lines[line]["max_shift"]
		lines[line]["text"] = text_wrap_word(lines[line]["raw_text"], lines[line]["shift"])

	return lines

# Updates shift lines with brand new data and resets shift
def new_text_wrap_word(data, lines):
	for line in lines:
		n_word = len(data[line].split(" "))
		# single word or short titles are not wrapped
		if n_word == 1 or len(data[line]) < 19:
			lines[line]["max_shift"] = 1
			lines[line]["raw_text"] = data[line]
		else:
			lines[line]["max_shift"] = n_word+1
			lines[line]["raw_text"] = data[line]+"        "
		
		lines[line]["text"] = lines[line]["raw_text"]
		lines[line]["shift"] = 0

	return lines