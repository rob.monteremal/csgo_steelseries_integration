from src.utils.utils import log

from imageio import imread
import numpy as np

def get_number(num):
	numbers = [
			# 0
		[ 	[0,0,1,1,1,0],
			[0,1,1,1,1,1],
			[0,1,0,0,1,1],
			[0,1,0,0,1,1],
			[0,1,0,0,1,1],
			[0,1,0,0,1,1],
			[0,1,0,0,1,1],
			[0,1,1,0,1,1],
			[0,0,1,1,1,0]
		],	# 1
		[ 	[0,0,0,1,1,0],
			[0,0,1,1,1,0],
			[0,1,1,1,1,0],
			[0,1,0,1,1,0],
			[0,0,0,1,1,0],
			[0,0,0,1,1,0],
			[0,0,0,1,1,0],
			[0,0,0,1,1,0],
			[0,0,0,1,1,0],
		],	# 2
		[ 	[0,0,1,1,1,0],
			[0,1,1,1,1,1],
			[0,1,0,0,0,1],
			[0,0,0,0,1,1],
			[0,0,0,1,1,1],
			[0,0,1,1,1,0],
			[0,1,1,1,0,0],
			[0,1,1,1,1,1],
			[1,1,1,1,1,1]
		],	# 3
		[ 	[0,0,1,1,1,0],
			[0,1,1,1,1,1],
			[0,1,0,0,1,1],
			[0,0,0,1,1,1],
			[0,0,0,1,1,0],
			[0,0,0,0,1,1],
			[0,1,0,0,0,1],
			[0,1,1,0,1,1],
			[0,0,1,1,1,0]
		],	# 4
		[ 	[0,0,0,0,1,0],
			[0,0,0,1,1,0],
			[0,0,1,1,1,0],
			[0,1,1,1,1,0],
			[0,1,0,1,1,0],
			[1,1,0,1,1,0],
			[1,1,1,1,1,1],
			[0,0,0,1,1,0],
			[0,0,0,1,1,0]
		],	# 5
		[ 	[0,1,1,1,1,1],
			[0,1,1,1,1,1],
			[0,1,0,0,0,0],
			[0,1,1,1,1,0],
			[1,1,1,1,1,1],
			[0,0,0,0,1,1],
			[0,0,0,0,1,1],
			[1,1,0,0,1,1],
			[0,1,1,1,1,0]
		],	# 6
		[ 	[0,0,1,1,1,0],
			[0,1,1,1,1,1],
			[1,1,0,0,0,0],
			[1,1,1,1,0,0],
			[1,1,1,1,1,0],
			[1,1,0,0,1,1],
			[1,1,0,0,1,1],
			[0,1,1,0,1,1],
			[0,1,1,1,1,0]
		],	# 7
		[ 	[1,1,1,1,1,1],
			[1,1,1,1,1,1],
			[0,0,0,1,1,0],
			[0,0,0,1,0,0],
			[0,0,1,1,0,0],
			[0,0,1,1,0,0],
			[0,0,1,0,0,0],
			[0,1,1,0,0,0],
			[0,1,1,0,0,0]
		],	# 8
		[ 	[0,1,1,1,1,0],
			[1,1,1,1,1,0],
			[1,1,0,0,1,1],
			[0,1,1,1,1,0],
			[0,1,1,1,1,0],
			[1,1,0,0,1,1],
			[1,1,0,0,1,1],
			[1,1,0,0,1,1],
			[0,1,1,1,1,0]
		],	# 9
		[ 	[0,1,1,1,0,0],
			[1,1,1,1,1,0],
			[1,1,0,0,1,1],
			[1,1,0,0,1,1],
			[1,1,0,1,1,1],
			[0,1,1,1,1,1],
			[0,0,0,0,1,1],
			[1,1,0,1,1,0],
			[0,1,1,1,0,0]
		]
	]

	try:
		return np.array(numbers[int(num)])
	except:
		return np.zeros((9,6))

def image_to_bitmap(path_image):
	im = imread(path_image)
	bitmap = np.zeros((im.shape))
	for y in range(len(im)):
		for x in range(len(im[0])):
			if im[y,x] < 128:
				bitmap[y,x] = 1
	return bitmap

def insert_in_bitmap(bitmap, mask, data):
	bitmap[mask] = data.flatten()
	return bitmap

def get_mask(mask_name):
	mask = np.zeros((40,128), dtype=bool)
	digit_mask = np.ones((9,6), dtype=bool)

	if mask_name == "FIRST_DIGIT":
		mask[31:40,90:96] = digit_mask
	elif mask_name == "SECOND_DIGIT":
		mask[31:40,97:103] = digit_mask
	elif mask_name == "THIRD_DIGIT":
		mask[31:40,104:110] = digit_mask
	elif mask_name == "AVG_FIRST_DIGIT":
		mask[31:40,30:36] = digit_mask
	elif mask_name == "AVG_SECOND_DIGIT":
		mask[31:40,37:43] = digit_mask
	elif mask_name == "AVG_THIRD_DIGIT":
		mask[31:40,44:50] = digit_mask
	elif mask_name == "MAIN_AREA":
		mask[0:29,0:127] = np.ones((128, 30), dtype=bool)
	else:
		log("Invalid mask name")

	return mask

def value_to_3digit(value):
	string = str(int(value))
	if len(string) == 1:
		out_string = "__"+string
	elif len(string) == 2:
		out_string = "_"+string
	elif len(string) == 3:
		out_string = string
	else:
		out_string = string

	return out_string

def ping_values_to_bitmap(bitmap, ping_avg, ping_value):
	values = value_to_3digit(ping_value)+value_to_3digit(ping_avg)
	insert_ping_bitmap(bitmap, values)
	return bitmap

def insert_ping_bitmap(bitmap, values):
	mask_names = ["FIRST_DIGIT", "SECOND_DIGIT", "THIRD_DIGIT", "AVG_FIRST_DIGIT", "AVG_SECOND_DIGIT", "AVG_THIRD_DIGIT"]
	for i in range(6):
		insert_in_bitmap(bitmap, get_mask(mask_names[i]), get_number(values[i]))

# converts ping value to screen height position
def ping_to_graph(value):
	top_bound = 0
	mid_bound = 9
	bottom_bound = 29

	if value <= 10:
		return bottom_bound
	elif value > 10 and value <= 70:
		return int(bottom_bound - (value-10)/3)
	elif value > 70 and value <= 199:
		return int(mid_bound - (value-70)/13)
	elif value >= 200:
		return top_bound
	else:
		return top_bound

# calculates new ghosts points
def get_new_ghost_points(p1, p2):
	if p1 == None:
		return []
	# points are consecutive
	if p1[0] == p2[0]:
		return []
	# downwards line
	elif p1[0] > p2[0]:
		dir = -1
	# upwards line
	else:
		dir = 1

	return [(y, p2[1]) for y in range(p1[0], p2[0], dir)]
	
# updates previous ghost points
def update_ghost_points(g_points):
	new_g_points = []
	for (y,x) in g_points:
		# out-of-bound points are removed
		if not(x < 1):
			# shits every point one pixel left
			new_g_points.append((y, x-1))

# calculates "ghost points" to link up following values
def get_ghost_points(g_points, p1, p2):
	update_ghost_points(g_points)
	return g_points + get_new_ghost_points(p1, p2)

# inserts every point in the list on the bitmap
def insert_graph_in_bitmap(bitmap, points, g_points):
	old_point = None
	for x, value in enumerate(points[::-1]):
		# draws ping value points
		point = (ping_to_graph(value), -(x+1))
		bitmap[point] = 1
		# links points together
		for p in get_ghost_points(g_points, old_point, point):
			bitmap[p] = 1	

		old_point = point

	return bitmap, g_points

def bitmap_to_steelseries(bitmap):
	image_data = []
	byte = 0
	for pos, bit in enumerate(bitmap.flatten()):
		byte += int(bit) << (7-pos%8)
		if pos % 8 == 7:
			image_data.append(byte)
			byte = 0

	return image_data


if __name__ == "__main__":
	bitmap = image_to_bitmap("Bitmap_reference\\ping_background.png")
	print(bitmap_to_steelseries(insert_in_bitmap(bitmap, get_mask("FIRST_DIGIT"), get_number(5))))