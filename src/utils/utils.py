import traceback
import json
from datetime import datetime
from imageio import imread

# converts a 18x32 greyscale image into the line of lisp code
# describing said image as an icon with the associated icon id


def convert_logo(path_image, id):
    im = imread(path_image)
    print("Size: %d x %d" % (len(im), len(im[0])))
    lines_string = ""
    for line in im:
        line_string = "["
        values = [0, 0, 0, 0]
        i = 0
        for pos, pixel in enumerate(line[::-1]):
            # dark pixel = 0 as a bit
            # bright pixel = 1
            if sum(pixel) < 255:
                values[i] += (1 << pos % 8)
            # every 8 pixels, we group them as a byte
            if pos % 8 == 7:
                i += 1
        line_string = "[%d %d %d %d] " % (
            values[3], values[2], values[1], values[0])
        lines_string += line_string

    lisp_string = "((%d) (let ((spacing (get-icon-top-bottom-spacing screen-height 18))) (append (make-list (first spacing) [0 0 0 0]) '(%s) (make-list (second spacing) [0 0 0 0]))))" % (
        id, lines_string)
    return lisp_string


def log(message, object=None):
    if isinstance(message, Exception):
        traceback.print_exc()
        return

    try:
        if object == None:
            raise Exception
        name = object.__class__.__name__
    except:
        name = ""

    print("[%s - %s] %s" % (datetime.now().strftime("%H:%M:%S"), name, message))


def load_file(path):
    try:
        with open(path) as file:
            return json.load(file)
    except Exception as e:
        log(e)
        return {}


if __name__ == "__main__":
    print(convert_logo("Icons\\Windows_icon.png", 27))
