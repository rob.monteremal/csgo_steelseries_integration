from src.utils.utils import log

from win32gui import PumpMessages  # pylint: disable=no-name-in-module
from keyboard import hook

import threading


class KeyboardListener(threading.Thread):

    def __init__(self, modules):
        threading.Thread.__init__(self)
        hook(self.OnKeyboardEvent)
        self.modules = modules
        self.debug = False

    # gets called on every key press
    def OnKeyboardEvent(self, event):
        # only listen to key presses down
        if self.debug:
            log(event.to_json, self)
        if event.event_type == "down":
            for module in self.modules:
                module.keyboard_event(event.name)

    def enableDebug(self):
        self.debug = True

    def run(self):
        PumpMessages()


if __name__ == "__main__":
    from win32gui import PumpMessages  # pylint: disable=no-name-in-module
    KeyboardListener([]).enableDebug()
    PumpMessages()
