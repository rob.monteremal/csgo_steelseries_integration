from src.utils.utils import log

import queue
import threading

import traceback


class CallQueue(threading.Thread):
    debug = False

    def __init__(self):
        threading.Thread.__init__(self)

        # shared queue
        self.q = queue.Queue()
        self.threads = {}

    # adds a thread in the dictionnary with the format {"thread_name": thread}
    def add_thread(self, thread):
        name = thread.__class__.__name__.lower()
        if self.debug:
            log("Adding "+name+" to CallQueue")
        self.threads.update({name: thread})

    # puts an item in the queue
    def put(self, source, dest, data):
        if self.debug:
            log("Putting data: "+str(data)+" into queue", self)
        self.q.put({"source": source, "dest": dest, "data": data})

    # processes task data and calls the destination thread
    def call_thread(self, item):
        try:
            self.threads[item["dest"]].queue_event(
                item["source"], item["data"])
        except Exception as e:
            log(item["dest"]+" isn't supported by the queue", self)
            log(e, self)
            if self.debug:
                traceback.print_exc()
            return None

    # constantly tries to empty the queue and process the data
    def run(self):
        while(True):
            item = self.q.get()
            try:
                if self.debug:
                    log(item, self)
                self.call_thread(item)
            except Exception as e:
                print("Could not process queue item!")
                print(e)
            finally:
                self.q.task_done()
