# other project files
import src.SteelSeries.steelseries as ss
from src.SteelSeries.pipeline import EventPipelineThread
from src.Modules.CSGO.csgo import CSGO
from src.Modules.Twitch.twitch_listener import TwitchListener
from src.Modules.Twitch.twitch import Twitch
from src.Modules.Spotify.spotify import Spotify
# from Modules.WindowsAudio.win_audio_wrapper import WinAudioWrapper
from src.Modules.Background.background import Background
from src.utils.KeyboardListener import KeyboardListener
from src.utils.utils import log

# from win32gui import PumpMessages # pylint: disable=no-name-in-module
import subprocess
import pickle
import codecs
from time import sleep

# Flask server
import flask
from flask import request

# modules to use
enable_CSGO = True
enable_SPOTIFY = True
enable_WINAUDIO = False
enable_BACKGROUND = True
enable_TWITCH = True

if __name__ == "__main__":
	threads = []
	addr = ss.get_addr()
	log("SteelSeries listening on port: %s" %(addr))
	listener = KeyboardListener(threads)
	listener.start()

	# OLED events pipeline
	pipeline_thread = EventPipelineThread(addr, 0.1)
	pipeline = pipeline_thread.get_pipeline()
	pipeline_thread.start()
	threads.append(pipeline_thread)

	if enable_CSGO:
		# CSGO server config
		csgo_app = flask.Flask("CSGO event listener")
		csgo_app.config["DEBUG"] = False

		# Receives CSGO game events generated with CSGO GSI
		@csgo_app.route("/csgo_game_events", methods=["GET","POST"])
		def csgo_event():
			return csgo_thread.incoming_data(request)

		# thread
		csgo_thread = CSGO(csgo_app, pipeline=(addr, pipeline))
		csgo_thread.start()
		threads.append(csgo_thread)

	if enable_TWITCH:
		# Server config
		twitch_app = flask.Flask("Twitch event listener")
		twitch_app.config["DEBUG"] = False

		# default route
		# @twitch_app.route('/', defaults={'path': ''})
		# @twitch_app.route('/<path:path>')
		# def default(path):
		# 	return None

		# receives Twitch authentification requests
		@twitch_app.route("/twitch", methods=["GET"])
		def auth_request():
			return twitch_listener_thread.incoming_data(request)

		# receives Twitch events generated with webhooks
		@twitch_app.route("/twitch/<topic>/<username>", methods=["GET", "POST"])
		def webhooks(topic, username):
			return twitch_listener_thread.incoming_data(request, topic, username)

		# thread
		twitch_thread = Twitch(pipeline=(addr, pipeline))
		twitch_thread.start()

		twitch_listener_thread = TwitchListener(twitch_app, twitch_thread, pipeline=(addr, pipeline))
		twitch_listener_thread.start()
		threads.append(twitch_thread)

	if enable_SPOTIFY:
		# thread
		spotify_thread = Spotify(pipeline=(addr, pipeline))
		spotify_thread.start()
		threads.append(spotify_thread)

	if enable_WINAUDIO:
		pass
		# # thread
		# win_audio_thread = WinAudioWrapper(addr, pipeline_thread)
		# win_audio_thread.start()
		# threads.append(win_audio_thread)

		# subprocess
		# pipeline_pickled = codecs.encode(pickle.dumps(pipeline_thread.get_pipeline()), "base64").decode()
		# win_audio_process = subprocess.Popen(
		# 	["python.exe", "Modules\\WindowsAudio\\win_audio_wrapper.py", pipeline_pickled]
		# )
		# win_audio_process = subprocess.Popen(
		# 	["python", "Modules\\WindowsAudio\\win_audio_wrapper.py"],
		# 	stdin=subprocess.PIPE, stdout=subprocess.PIPE, bufsize=0
		# )
		# pickle.dump(pipeline_thread.get_pipeline(), win_audio_process.stdin)
		# win_audio_thread = pickle.load(win_audio_process.stdout)

	if enable_BACKGROUND:
		# thread
		bg_thread = Background(pipeline=(addr, pipeline))
		bg_thread.start()
		threads.append(bg_thread)
