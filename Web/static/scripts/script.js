/* Utilities */
time_string = function(time)	{
	var min =  Math.floor(time/60);
	var sec =  Math.floor(time%60);
	if (sec < 10)	{
		return min+":0"+sec;
	} else {
		return min+":"+sec;
	}
}

/* SPOTIFY */
spotify_player_handler = function (data) {
	try	{
		document.getElementById("spotify-player-track-name").innerHTML = data.track_name;
		document.getElementById("spotify-player-artist-name").innerHTML = data.artist_name;
		document.getElementById("spotify-player-album-name").innerHTML = data.album_name;
		document.getElementById("spotify-player-album-cover").innerHTML = '<img src='+data.album_cover+' alt="Album cover">'
	} catch (error) {
		console.log('Error in ', arguments.callee.name);
		console.log(error);
	}
};
spotify_playlist_handler = function (playlists)	{
	html = "";
	for (playlist of playlists)	{
		html += '<div class="expand-text spotify-text hitbox unselectable" id="spotify-playlist" data-uri="'+playlist.uri+'" onclick="spotify_play_uri(this.getAttribute('+"'data-uri'"+'))">'+playlist.name+'</div>';
	}
	document.getElementById("spotify-playlist-container").innerHTML = html;
}
spotify_progress_handler = function (data)	{
	// console.log(data.progress);
	document.getElementById("spotify-time-left").innerHTML = time_string(data.time);
	document.getElementById("spotify-time-right").innerHTML = time_string(data.duration);
	document.getElementById("spotify-progress-innerbar").style.width = data.progress + '%';	
}
spotify_controls_handler = function (data)	{
	spotify_set_volume(data.volume);
	spotify_set_image(document.getElementById("spotify-control-pause-img"), data.icon);
	spotify_set_image(document.getElementById("spotify-control-shuffle-img"), data.shuffle);
	spotify_set_image(document.getElementById("spotify-control-loop-img"), data.loop);
	spotify_set_image(document.getElementById("spotify-control-save-img"), data.save);
}

spotify_handler = function(data) {
	if ("player" in data)	{
		spotify_player_handler(data.player);
	}
	if ("playlists" in data)	{
		spotify_playlist_handler(data.playlists);
	}
	if ("progress" in data)	{
		spotify_progress_handler(data.progress);
	}
	if ("controls" in data)	{
		spotify_controls_handler(data.controls);
	}
};

spotify_control = function(e)	{
	send("spotify-update", {"action": e});
}

spotify_set_image = function(e, state)	{
	e.alt = state;
	e.src = "static/images/spotify/"+state+".png";
}
spotify_save_toggle = function(e)	{
	send("spotify-update", {"action": e.alt});
	if (e.alt == "save")	{
		spotify_set_image(e, "unsave");
	} else {
		spotify_set_image(e, "save");
	}
}
spotify_pause_toggle = function(e) {
	send("spotify-update", {"action": e.alt});
	if (e.alt == "play")	{
		spotify_set_image(e, "pause");
	}
	else {
		spotify_set_image(e, "play");
	}
};
spotify_shuffle_toggle = function(e)	{
	if (e.alt == "shuffle_on")	{
		spotify_set_image(e, "shuffle_off");
	} else {
		spotify_set_image(e, "shuffle_on");
	}
	send("spotify-update", {"action": e.alt});
}
spotify_loop_toggle = function(e)	{
	if (e.alt == "loop_context")	{
		spotify_set_image(e, "loop_track");
	} else if (e.alt == "loop_track") {
		spotify_set_image(e, "loop_off");		
	} else {
		spotify_set_image(e, "loop_context");
	}
	send("spotify-update", {"action": e.alt});
}
spotify_play_uri = function(uri)	{
	send("spotify-update", {"uri": uri});
}

// Spotify progress bar
document.getElementById("spotify-progress-hitbox").addEventListener('click', function(e) {
	var percentage = (e.offsetX) / this.offsetWidth * 100;
	spotify_set_progress(percentage);
	send("spotify-update", {"progress_percent" : percentage});
});
spotify_set_progress = function(progress)	{
	progressBarValue = document.getElementById("spotify-progress-innerbar");
	progressBarValue.style.width = progress + '%';
	progressBarValue.innerHTML = "Progress"+progress+"%";
}

// Spotify volume bar
document.getElementById("spotify-volume-hitbox").addEventListener('click', function(e) {
	var percentage = (e.srcElement.offsetHeight - e.offsetY) / this.offsetHeight * 100;
	spotify_set_volume(percentage);
	send("spotify-update", {"volume_percent" : percentage});
});
spotify_set_volume = function(volume)	{
	volumeBarValue = document.getElementById("spotify-volume-innerbar");
	volumeImage = document.getElementById("spotify-control-mute-img");
	volumeBarValue.style.height = volume + '%';
	volumeBarValue.innerHTML = "VOLUME"+volume+"%";
	if (volume == 0)	{
		volumeImage.src = "static/images/spotify/volume_mute.png";
	} else if (volume < 33)	{
		volumeImage.src = "static/images/spotify/volume_low.png";
	} else if (volume < 67)	{
		volumeImage.src = "static/images/spotify/volume_med.png";
	} else {
		volumeImage.src = "static/images/spotify/volume_high.png";
	}
}

/* Notifications */
craft_notification = function(data)	{
	return '<div class="notification expand-text tile-container" id='+data.id+'><div class="tile-col tile-fix" id="notification-image"><img src='+data.image_url+' style="border-color: '+data.color+';" alt="Twitch logo"></div><a class="tile-col" id="notification-text" href="'+data.link+'" target="_blank"><div class="one-line expand-text unselectable" id="notification-title">'+data.data.username+' just went live</div><div class="one-line expand-text unselectable" id="notification-subtitle">Playing: '+data.data.game+'</div><div class="unselectable expand-text text" id="notification-full-text">'+data.data.title+'</div></a><div class="tile-col tile-fix" style="position: relative;" id="notification-controls" onclick="clear_notification(this);"><div id="notification-dismiss"><div class="hitbox" id="notification-dismiss-image"><img src="/static/images/notification/dismiss.png"></div></div><div class="notficiation-datetime unselectable" style="bottom: 1.5em;">'+data.date+'</div><div class="notficiation-datetime unselectable" style="bottom: 0;">'+data.time+'</div></div></div>';
}

notificationContainer = document.getElementById("notification-container");
add_notification = function(data)	{
	notification = craft_notification(data);
	notificationContainer.innerHTML = notification += notificationContainer.innerHTML;
}
clear_notification = function(notification)	{
	send("notification-clear", notification.parentNode.id);
	notification.parentNode.remove();
}

/* Weather widget */
weatherHours = document.getElementById("weather-hours");
weatherDays = document.getElementById("weather-days");
weather_handler = function(weather_data)	{
	for (let i=0; i<weatherHours.children.length; i++)	{
		weatherHours.children[i].getElementsByTagName("img")[0].src = weather_data.hours[i].weather_icon;
		weatherHours.children[i].getElementsByTagName("img")[0].alt = weather_data.hours[i].weather;
		weatherHours.children[i].getElementsByClassName("temp")[0].innerHTML = "🌡"+Math.round(weather_data.hours[i].temp)+"°C";
		weatherHours.children[i].getElementsByClassName("rain")[0].innerHTML = "💧"+Math.round(weather_data.hours[i].rain_prob*100)+"%";
		weatherHours.children[i].getElementsByClassName("time")[0].innerHTML = weather_data.hours[i].hour+"h";
	}
	for (let i=0; i<weatherHours.children.length; i++)	{
		weatherDays.children[i].getElementsByTagName("img")[0].src = weather_data.days[i].weather_icon;
		weatherDays.children[i].getElementsByTagName("img")[0].alt = weather_data.days[i].weather;
		weatherDays.children[i].getElementsByClassName("temp-low")[0].innerHTML = "🌡"+Math.round(weather_data.days[i].temp.min)+"°C";
		weatherDays.children[i].getElementsByClassName("temp-high")[0].innerHTML = "🌡"+Math.round(weather_data.days[i].temp.max)+"°C";
		weatherDays.children[i].getElementsByClassName("date")[0].innerHTML = weather_data.days[i].date;
	}
}

logitech_handler = function(logitech_data)	{
	for (peripheral of Object.keys(logitech_data))	{
		element = document.getElementById(peripheral).getElementsByClassName("battery")[0];
		element.innerHTML = logitech_data[peripheral].charging ? "⚡ " : " ";
		element.innerHTML += logitech_data[peripheral].battery + " % ";
	}
}

// General handler
update_handler = function(event) {
	console.log(event);
	for (source of Object.keys(event))	{
		switch(source)	{
			case "spotify":
				spotify_handler(event.spotify);
				break;
			case "notifications":
				for (notification of event.notifications)	{
					add_notification(notification);
				}
				break;
			case "weather":
				weather_handler(event.weather);
				break;
			case "logitech":
				logitech_handler(event.logitech);
				break;
			default:
				console.log('Unhandled update: '+source);
				break;
		}
	}
};

// Websocket
var socket = io();
var session_ID = Math.floor(Math.random() * 1000000);
socket.on('connect', function() {
	socket.emit('connected', {data: session_ID});
});

// Send data through websocket
send = function(type, data)	{
	try {
		socket.emit(type, data);
	} catch (error)	{
		console.log("Error while sending "+type);
		console.log(error);
	}
}

// Event listeners
socket.addEventListener('update', update_handler);

// After page is loaded
$(document).ready(function(){
	console.log("Page loaded!");
	send("start", null);
});